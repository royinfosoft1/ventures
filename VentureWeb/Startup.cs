using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VentureWeb.DAL;

/// Added by Sayantani
//using System.Diagnostics;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Session_State.Models;
///Ended by Sayantani


namespace VentureWeb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IUseraccess, Useraccess>();
            services.AddScoped<IProductCategoryaccess, ProductCategoryaccess>();
            services.AddScoped<IProductaccess, Productaccess>();            
            services.AddScoped<ICompanyaccess, Companyaccess>();
            services.AddScoped<IProductClassAccess, ProductClassAccess>();
            services.AddScoped<IProductRateaccess, ProductRateaccess>();
            services.AddScoped<IUnitaccess, Unitaccess>();
            services.AddScoped<IImageDetailsaccess, ImageDetailsaccess>();
            services.AddScoped<ILedgerAccess, LedgerAccess>();
            services.AddControllersWithViews();

            /// For Activate session --Start Sayantani
            services.AddDistributedMemoryCache();
            services.AddSession(options => {
                options.IdleTimeout = TimeSpan.FromMinutes(20);//You can set Time   
            });
            services.AddMvc();
            /// For Activate session --End Sayantani
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink(); ///added by sayantani
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseSession();/// added by sayantani

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    //pattern: "{controller=Home}/{action=Home}/{id?}");
                    pattern: "{controller=User}/{action=UserLogin}/{id?}");
            });
        }

       
    }
}
