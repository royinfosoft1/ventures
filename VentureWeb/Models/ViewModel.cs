﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using VentureWeb.Models;


namespace VentureWeb.ViewModel
{
    
    public class DefaultProductListViewModel
    {
        public List<ProductCategory> Categories { get; set; }
        public List<ProductViewModel> Products { get; set; }
    }

    public class ProductViewModel:Product
    {
        public string DefaultImagePath { get; set; }
        public string ProductCategory { get; set; }
        public string ProductClass { get; set; }

       
    }
    public class ListProductViewModel
    {
        public List<ProductViewModel> productList { get; set; }
        public List<ProductCategory> ProductCategoryList { get; set; }
        public List<ProductClass> ProductClassList { get; set; }
    }

    public class ProductPicturesViewModel 
    {
        public Product Product { get; set; }
        public List<ImageDetails> ProductImages { get; set; }
        public List<ImageDetails> WcareImages { get; set; }
    }

    public class UserViewModel
    {
        public Registration Registration { get; set; }
        public List<SelectListItem> UserState { get; set; }

    }

    public class AddEditProductViewModel
    {
        public List<ProductCategory> ProductCategory { get; set; }
        public List<ProductClass> ProductClass { get; set; }
        public List<Unit> Unit { get; set; }
       // public List<UnitMethod> UnitMethod { get; set; }
        public List<ImageDetails> ProductImages { get; set; }
        public List<ImageDetails> WcareImages { get; set; }
        public Product Product { get; set; }
    }

    public class LedgerGroupVM : LedgerGroup
    {
        public string ParentGroupName { get; set; }
        
    }

    public class LedgerNameVM: Ledger
    {
        public string LedgerGroupName { get; set; }      
        
    }

    public class AddEditLedgerNameVM
    {
        public LedgerNameVM Ledger { get; set; }
        public List<LedgerGroup> LedgerGroup { get; set; }
    }

    public class SubLedgerClassVM : SubLedgerClass
    {
        public string ParentClassName { get; set; }

    }
}
