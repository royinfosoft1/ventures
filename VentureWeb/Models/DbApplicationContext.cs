﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace VentureWeb.Models
{
    public class DbApplicationContext : DbContext
    {
        public DbApplicationContext()
        {
        }

        public DbApplicationContext(DbContextOptions<DbContext> options) : base(options)
        {
        }
        public DbSet<Registration> Registration { get; set; }

        public DbSet<ProductCategory> ProductCategory { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductClass> ProductClass { get; set; }

        public DbSet<ProductRate> ProductRate { get; set; }

        public DbSet<Company> Company { get; set; }

        public DbSet<Unit> Unit { get; set; }

        public DbSet<ImageDetails> ImageDetails { get; set; }

        public DbSet<LedgerGroup> LedgerGroup { get; set; }

        public DbSet<Ledger> Ledger { get; set; }

        public DbSet<SubLedgerClass> SubLedgerClass { get; set; }



        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Host=157.245.103.40;Database=VenturesDb;Username=postgres;Password=Ris123###");
            }
        }
    }
}
