﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace VentureWeb.Models
{
    public class Registration
    {

        public string Name { get; set; }
        public string Designation { get; set; }
        public string ReportingTo { get; set; }
        public string EmailId { get; set; }
        [Key]
        public long EmpId { get; set; }
        public string EmpNo { get; set; }
        public string PhoneNo { get; set; }
        public string Password { get; set; }
        public int UserStatus { get; set; }

}

    public class UserStateModel
    {
        public long UserStatusId { get; set; }
        public long UserStatusValue { get; set; }
    }

    public class ProductCategory
    {
        [Key]
        public long ProductCategoryId { get; set; }
        public string ProductCategoryName { get; set; }

    }

    public class Product
    {
        public long CompId { get; set; }
        [Key]
        public long ProductId { get; set; }
        public long ProductClassId { get; set; }
        public long ProductCategoryId { get; set; }
        public string ProductName { get; set; }
        public string Width { get; set; }
        public int Width1 { get; set; }
        public int Width2 { get; set; }
        public int Width3 { get; set; }
        public int WidthTotal { get; set; }
        public string ReferenceCode { get; set; }
        public string ITCCode { get; set; }
        public string BarCode { get; set; }
        public int ValidationMethod { get; set; }
        public int OpeningUnitId { get; set; }
        public long AlertUnitId { get; set; }
        public int AlertUnitMethod { get; set; }
        public decimal AlertUnitFactor { get; set; }
        public long SaleUnitId { get; set; }
        public int FreeQuantityTag { get; set; }
        public decimal MinimumLevel { get; set; }
        public decimal MaximunLevel { get; set; }
        public decimal ReorderLevel { get; set; }
        public decimal DefaultPurchaseRate { get; set; }
        public decimal DefaultSateRate { get; set; }
        public decimal MinimumSaleRate { get; set; }
        public decimal MaximumSaleRate { get; set; }
        public decimal TaxPercent { get; set; }
        public int IsFinishedProduct { get; set; }
        public long EntryUserId { get; set; }
        public DateTime EntryUserDate { get; set; }
        public long UpdateUserId { get; set; }
        public DateTime UpdateUserDate { get; set; }
        public bool IsFeatured { get; set; }

    }

    //fqdn of Image is ImagePath+Imagename+ImageExt
    public class ImageDetails
    { 
        [Key]
        public long ImageId { get; set; }
      
        //what type of image product,user etc
        public String ImageType { get; set; }

        //fkey with relevent table
        public long LinkId { get; set; }
        public string ImagePath { get; set; }
        public string ImageName { get; set; }
        public string ImageExt { get; set; }
        public string ImageDesc { get; set; }
        public bool IsDefault { get; set; }
        
    }

    public class ProductClass
    {
        public int CompId { get; set; }
        [Key]
        public long ProductClassId { get; set; }
        public string ProductClassName { get; set; }
        public long UnderId { get; set; }
        public string IsParent { get; set; }
        public string ParentString { get; set; }
        public int IsMain { get; set; }
        public long EntryUserId { get; set; }
        public DateTime EntryUserDate { get; set; }
        public long UpdateUserId { get; set; }
        public DateTime UpdateUserDate { get; set; }

    }

    public class Company
    {
        [Key]
        public long CompId { get; set; }
        public string CompName { get; set; }
        public string CompShortName { get; set; }
        public string CompPerAdd1 { get; set; }
        public string CompPerAdd2 { get; set; }
        public string CompPerAdd3 { get; set; }
        public string CompMailAdd1 { get; set; }
        public string CompMailAdd2 { get; set; }
        public string CompMailAdd3 { get; set; }
        public string CompPanNo { get; set; }
        public string CompTanNo { get; set; }
        public string CompListNo { get; set; }
        public string CompCstNo { get; set; }
        public string CompVatFlg { get; set; }
        public DateTime CompBookMstDt { get; set; }
        public string CompPhone1 { get; set; }
        public string CompPhone2 { get; set; }
        public string CompPhone3 { get; set; }
        public string CompPhone4 { get; set; }
        public string CompFaxNo1 { get; set; }
        public string CompFaxNo2 { get; set; }
        public string CompEmailId1 { get; set; }
        public string CompEmailId2 { get; set; }
        public string CompWebsite { get; set; }
        public string CompStatus { get; set; }
        public long EntryUserId { get; set; }
        public DateTime EntryUserDate { get; set; }
        public long UpdateUserId { get; set; }
        public DateTime UpdateUserDate { get; set; }

    }

    public class Unit
    {
        public long CompId { get; set; }

        [Key]
        public long UnitId { get; set; }
        public string UnitName { get; set; }
        public string UnitDescription { get; set; }
        public int UnitDecimalPlace { get; set; }
        public long EntryUserId { get; set; }
        public DateTime EntryUserDate { get; set; }
        public long UpdateUserId { get; set; }
        public DateTime UpdateUserDate { get; set; }

    }

    public class ProductRate
    {
        public long CompId { get; set; }
        public long ProductId { get; set; }
        [Key]
        public long RateId { get; set; }
        public DateTime EffectDate { get; set; }
        public decimal Rate { get; set; }
        public int Status { get; set; }
        public decimal DyeRate { get; set; }
        public decimal TotalRate { get; set; }
        public long EntryUserId { get; set; }
        public DateTime EntryUserDate { get; set; }
        public long UpdateUserId { get; set; }
        public DateTime UpdateUserDate { get; set; }

    }

    public class Message
    {
        public string status { get; set; }
        public string Msg { get; set; }
        public string body { get; set; }
        
    }

    public class LedgerGroup
    {
        
        public long CompId { get; set; }
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity), Key()]
        public long LedgerGroupId { get; set; }
        public string LedgerGroupName { get; set; }
        public long UnderId { get; set; }
        public string IsParent { get; set; }
        public string Parentstring { get; set; }
        public int IsMain { get; set; }
        public int IsEditable { get; set; }
        public int IsAddress { get; set; }
        public int IsTrading { get; set; }
        public string GroupType { get; set; }
        public int IsSubledger { get; set; }
        public int IsMiscelaneous { get; set; }
        public string ShowLedgergroup { get; set; }
        public int Is_Pl { get; set; }
        public long EntryUserId { get; set; }
        public DateTime EntryUserDate { get; set; }
        public long UpdateUserId { get; set; }
        public DateTime UpdateUserDate { get; set; }

    }
    public class Ledger
    {
    public long CompId { get; set; }
    [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity), Key()]
    public long LedgerId { get; set; }
    public string LedgerName { get; set; }
    public long LedgerGroupId { get; set; }
    public int InventoryEffect { get; set; }
    public int IsMain { get; set; }
    public int IsSubledger { get; set; }
    public long AgentId { get; set; }
    public long CountryId { get; set; }
    public string Accountent { get; set; }
    public string Ledgeraddress1 { get; set; }
    public string Ledgeraddress2 { get; set; }
    public string Ledgeraddress3 { get; set; }
    public string Ledgerphone1 { get; set; }
    public string Ledgerphone2 { get; set; }
    public string Ledgerfax { get; set; }
    public string Ledgeremail { get; set; }
    public string LedgerWebsite { get; set; }
    public string LedgerVatNo { get; set; }
    public string LedgerPanNo { get; set; }
    public string BankBranch { get; set; }
    public string Interestmethod { get; set; }
    public decimal InterestPtc { get; set; }
    public long SbuLedgerTypeId { get; set; }
    public string SubledgerDestination { get; set; }
    public long SubledgerClassId { get; set; }
    public string SubledgerContactPerson { get; set; }
    public string SubledgerCstNo { get; set; }
    public int SubledgerCreditDays { get; set; }
    public int SubledgerCreditLimit { get; set; }
    public string taxNature { get; set; }
    public string Showledger { get; set; }
    public DateTime DateOfBirth { get; set; }
    public DateTime DateOfMarraige { get; set; }
    public int manualStopBuilding { get; set; }
    public int DeducteeType { get; set; }
    public long StateId { get; set; }
    public long SectionId { get; set; }
    public int OnlyAccounts { get; set; }
    public long EntryUserId { get; set; }
    public DateTime EntryUserDate { get; set; }
    public long UpdateUserId { get; set; }
    public DateTime UpdateUserDate { get; set; }

    }

    public class SubLedgerClass
    {
        public long CompId { get; set; }


        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity), Key()]
        public long SubLedgerClassId { get; set; }
        public string SubLedgerClassName { get; set; }
        public long UnderId { get; set; }
        public string IsParent { get; set; }
        public string ParentString { get; set; }
        public int IsMain { get; set; }
        public int UnderLevel { get; set; }
        public long MainId { get; set; }
        public long EntryUserId { get; set; }
        public DateTime EntryUserDate { get; set; }
        public long UpdateUserId { get; set; }
        public DateTime UpdateUserDate { get; set; }
    }

}
