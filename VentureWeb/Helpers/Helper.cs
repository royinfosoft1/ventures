﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VentureWeb.Helpers
{
    public enum UserStatus
    {
        Select=0,
        UnApproved = 1,
        Approved = 2,
        Canceled = 3,
        Suspended = 4,
        Excluded = 5
    }
}
