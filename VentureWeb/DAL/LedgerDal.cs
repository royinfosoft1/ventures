﻿using System;
using System.Collections.Generic;
using System.Linq;
using VentureWeb.Models;
using VentureWeb.ViewModel;

namespace VentureWeb.DAL
{

    public interface ILedgerAccess
    {

        #region LedgerGroup
        long AddLedgerGroup(LedgerGroup model);
        long EditLedgerGroup(LedgerGroup model);
        List<LedgerGroup> LedgerGroupList(int pageNo, int PageCount);
        bool DeleteLedgerGroup(long LedgerGroupId);
        LedgerGroupVM GetLedgerGroup(long LedgerGroupId);

        LedgerGroupVM GetParentLedgerGroup(long LedgerGroupId);

        #endregion LedgerGroup

        #region LedgerName
        long AddLedgerName(Ledger model);
        long EditLedgerName(Ledger model);
        List<LedgerNameVM> LedgerNameList(int pageNo, int PageCount);
        bool DeleteLedgerName(long LedgerId);
        Ledger GetLedgerName(long LedgerId);
        LedgerNameVM GetLedgerDetails(long LedgerGroupId);

        #endregion LedgerName

        #region SubLedgerClass

        long AddSubLedgerClass(SubLedgerClass model);
        long EditSubLedgerClass(SubLedgerClass model);
        List<SubLedgerClass> SubLedgerClassList(int pageNo, int PageCount);
        bool DeleteSubLedgerClass(long SubLedgerClassId);
        SubLedgerClass GetSubLedgerClass(long SubLedgerClassId);
        SubLedgerClassVM GetParentSubLedgerClass(long SubLedgerClassId);

        #endregion SubLedgerClass



    }
    public class LedgerAccess : ILedgerAccess
    {

        #region LedgerGroup

        public long AddLedgerGroup(LedgerGroup model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {


                    if (context.LedgerGroup.Any(x => x.LedgerGroupId == model.LedgerGroupId))
                        throw new Exception("LedgerGroupId\"" + model.LedgerGroupId + "\" is already taken");
                    if (context.LedgerGroup.Any(x => x.LedgerGroupName == model.LedgerGroupName))
                        throw new Exception("LedgerGroupName\"" + model.LedgerGroupName + "\" is already taken");

                    context.LedgerGroup.Add(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return model.LedgerGroupId;

        }

        public bool DeleteLedgerGroup(long LedgerGroupId)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {                  
                        LedgerGroup model = context.LedgerGroup.Find(LedgerGroupId);
                        context.LedgerGroup.Remove(model);
                        context.SaveChanges();                    
                }

            }

            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        public long EditLedgerGroup(LedgerGroup model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {

                    /// get the LedgerGroup

                    var LedgerGroup = context.LedgerGroup.Find(model.LedgerGroupId);

                    LedgerGroup.LedgerGroupId = model.LedgerGroupId;
                    LedgerGroup.CompId = model.CompId;
                    LedgerGroup.LedgerGroupName = model.LedgerGroupName;
                    LedgerGroup.UnderId = model.UnderId;
                    LedgerGroup.IsParent = model.IsParent;
                    LedgerGroup.Parentstring = model.Parentstring;
                    LedgerGroup.IsMain = model.IsMain;
                    LedgerGroup.IsEditable = model.IsEditable;
                    LedgerGroup.IsAddress = model.IsAddress;
                    LedgerGroup.IsTrading = model.IsTrading;
                    LedgerGroup.GroupType = model.GroupType;
                    LedgerGroup.IsSubledger = model.IsSubledger;
                    LedgerGroup.IsMiscelaneous = model.IsMiscelaneous;
                    LedgerGroup.ShowLedgergroup = model.ShowLedgergroup;
                    LedgerGroup.Is_Pl = model.Is_Pl;
                    LedgerGroup.EntryUserId = model.EntryUserId;
                    LedgerGroup.EntryUserDate = model.EntryUserDate;
                    LedgerGroup.UpdateUserId = model.UpdateUserId;
                    LedgerGroup.UpdateUserDate = model.UpdateUserDate;

                    context.LedgerGroup.Update(LedgerGroup);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model.LedgerGroupId;
        }

        public LedgerGroupVM GetParentLedgerGroup(long LedgerGroupId)
        {
            LedgerGroupVM model = new LedgerGroupVM();
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    var ledgergroup = context.LedgerGroup.Find(LedgerGroupId);
                    
                    model.LedgerGroupName = ledgergroup.LedgerGroupName;
                    model.LedgerGroupId = ledgergroup.LedgerGroupId;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public LedgerGroupVM GetLedgerGroup(long LedgerGroupId)
        {
            LedgerGroupVM model = new LedgerGroupVM();
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    //var ledgergroup = context.LedgerGroup.Find(LedgerGroupId);
                    //var query = context.LedgerGroup.ToList();

                    var query = (from p in context.LedgerGroup where p.LedgerGroupId == LedgerGroupId
                                join q in context.LedgerGroup on p.UnderId equals q.LedgerGroupId
                    select new LedgerGroupVM
                    {
                        LedgerGroupId = p.LedgerGroupId,
                        LedgerGroupName = p.LedgerGroupName,
                        ParentGroupName = q.LedgerGroupName,
                        UnderId=p.UnderId,
                        CompId = p.CompId,                    
                        IsParent = p.IsParent,
                        Parentstring = p.Parentstring,
                        IsMain = p.IsMain,
                        IsEditable = p.IsEditable,
                        IsAddress = p.IsAddress,
                        IsTrading = p.IsTrading,
                        GroupType = p.GroupType,
                        IsSubledger = p.IsSubledger,
                        IsMiscelaneous = p.IsMiscelaneous,
                        ShowLedgergroup = p.ShowLedgergroup,
                        Is_Pl = p.Is_Pl,
                        EntryUserId = p.EntryUserId,
                        EntryUserDate = p.EntryUserDate,
                        UpdateUserId = p.UpdateUserId,
                        UpdateUserDate = p.UpdateUserDate

                }).FirstOrDefault();
                    model = query;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public List<LedgerGroup> LedgerGroupList(int pageNo, int PageCount)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    return context.LedgerGroup.OrderBy(t => t.LedgerGroupName).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion LedgerGroup

        #region LedgerName

        public long AddLedgerName(Ledger model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {


                    if (context.Ledger.Any(x => x.LedgerId == model.LedgerId))
                        throw new Exception("LedgerId\"" + model.LedgerId + "\" is already taken");
                    if (context.Ledger.Any(x => x.LedgerName == model.LedgerName))
                        throw new Exception("LedgerName\"" + model.LedgerName + "\" is already taken");

                    context.Ledger.Add(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return model.LedgerId;

        }

        public bool DeleteLedgerName(long LedgerId)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    Ledger model = context.Ledger.Find(LedgerId);
                    context.Ledger.Remove(model);
                    context.SaveChanges();
                }

            }

            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        public long EditLedgerName(Ledger model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {

                    /// get the LedgerGroup

                    var Ledger = context.Ledger.Find(model.LedgerId);


                    Ledger.CompId = model.CompId;
                    Ledger.LedgerId = model.LedgerId;
                    Ledger.LedgerName = model.LedgerName;
                    Ledger.LedgerGroupId = model.LedgerGroupId;
                    Ledger.InventoryEffect = model.InventoryEffect;
                    Ledger.IsMain = model.IsMain;
                    Ledger.IsSubledger = model.IsSubledger;
                    Ledger.AgentId = model.AgentId;
                    Ledger.CountryId = model.CountryId;
                    Ledger.Accountent = model.Accountent;
                    Ledger.Ledgeraddress1 = model.Ledgeraddress1;
                    Ledger.Ledgeraddress2 = model.Ledgeraddress2;
                    Ledger.Ledgeraddress3 = model.Ledgeraddress3;
                    Ledger.Ledgerphone1 = model.Ledgerphone1;
                    Ledger.Ledgerphone2 = model.Ledgerphone2;
                    Ledger.Ledgerfax = model.Ledgerfax;
                    Ledger.Ledgeremail = model.Ledgeremail;
                    Ledger.LedgerWebsite = model.LedgerWebsite;
                    Ledger.LedgerVatNo = model.LedgerVatNo;

                    context.Ledger.Update(Ledger);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model.LedgerGroupId;
        }

        public Ledger GetLedgerName(long LedgerId)
        {
            Ledger model = new Ledger();
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                        model = context.Ledger.Find(LedgerId); 
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public LedgerNameVM GetLedgerDetails(long LedgerId)
        {
            LedgerNameVM model = new LedgerNameVM();
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {                  

                    var query = (from p in context.Ledger
                                 where p.LedgerId == LedgerId
                                 join q in context.LedgerGroup on p.LedgerGroupId equals q.LedgerGroupId
                                 select new LedgerNameVM
                                 {
                                     LedgerId = p.LedgerId,
                                     LedgerGroupId = p.LedgerGroupId,
                                     LedgerGroupName = q.LedgerGroupName,
                                     LedgerName = p.LedgerName,
                                     InventoryEffect = p.InventoryEffect,
                                     CompId = p.CompId,
                                     IsMain = p.IsMain,
                                     IsSubledger = p.IsSubledger,
                                     AgentId = p.AgentId,
                                     CountryId = p.CountryId,
                                     Accountent = p.Accountent,
                                     Ledgeraddress1 = p.Ledgeraddress1,
                                     Ledgeraddress2 = p.Ledgeraddress2,
                                     Ledgeraddress3 = p.Ledgeraddress3,
                                     Ledgerphone1 = p.Ledgerphone1,
                                     Ledgerphone2 = p.Ledgerphone2,
                                     Ledgerfax = p.Ledgerfax,
                                     Ledgeremail = p.Ledgeremail,

                                     LedgerWebsite = p.LedgerWebsite,
                                     LedgerVatNo = p.LedgerVatNo,
                                     LedgerPanNo = p.LedgerPanNo,
                                     BankBranch = p.BankBranch,
                                     Interestmethod = p.Interestmethod,
                                     InterestPtc = p.InterestPtc,
                                     SbuLedgerTypeId = p.SbuLedgerTypeId,

                                     SubledgerDestination = p.SubledgerDestination,
                                     SubledgerClassId = p.SubledgerClassId,
                                     SubledgerContactPerson = p.SubledgerContactPerson,
                                     SubledgerCstNo = p.SubledgerCstNo,
                                     SubledgerCreditDays = p.SubledgerCreditDays,
                                     SubledgerCreditLimit = p.SubledgerCreditLimit,
                                     taxNature = p.taxNature,
                                     Showledger = p.Showledger,
                                     DateOfBirth = p.DateOfBirth,
                                     DateOfMarraige = p.DateOfMarraige,

                                     manualStopBuilding = p.manualStopBuilding,
                                     DeducteeType = p.DeducteeType,
                                     StateId = p.StateId,
                                     SectionId = p.SectionId,
                                     OnlyAccounts = p.OnlyAccounts,                                   


                                     EntryUserId = p.EntryUserId,
                                     EntryUserDate = p.EntryUserDate,
                                     UpdateUserId = p.UpdateUserId,
                                     UpdateUserDate = p.UpdateUserDate

                                 }).FirstOrDefault();
                    model = query;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public List<LedgerNameVM> LedgerNameList(int pageNo, int PageCount)
        {
            List<LedgerNameVM> model = new List<LedgerNameVM>();
            try
            {
               
                //using (DbApplicationContext context = new DbApplicationContext())
                //{
                    // return context.Ledger.OrderBy(t => t.LedgerName).ToList();

                    using (DbApplicationContext context = new DbApplicationContext())
                    {

                        var query = (from p in context.Ledger
                                   //  where p.LedgerId == LedgerId
                                     join q in context.LedgerGroup on p.LedgerGroupId equals q.LedgerGroupId
                                     select new LedgerNameVM
                                     {
                                         LedgerId = p.LedgerId,
                                         LedgerGroupId = p.LedgerGroupId,
                                         LedgerGroupName = q.LedgerGroupName,
                                         LedgerName = p.LedgerName,
                                         InventoryEffect = p.InventoryEffect,
                                         CompId = p.CompId,
                                         IsMain = p.IsMain,
                                         IsSubledger = p.IsSubledger,
                                         AgentId = p.AgentId,
                                         CountryId = p.CountryId,
                                         Accountent = p.Accountent,
                                         Ledgeraddress1 = p.Ledgeraddress1,
                                         Ledgeraddress2 = p.Ledgeraddress2,
                                         Ledgeraddress3 = p.Ledgeraddress3,
                                         Ledgerphone1 = p.Ledgerphone1,
                                         Ledgerphone2 = p.Ledgerphone2,
                                         Ledgerfax = p.Ledgerfax,
                                         Ledgeremail = p.Ledgeremail,

                                         LedgerWebsite = p.LedgerWebsite,
                                         LedgerVatNo = p.LedgerVatNo,
                                         LedgerPanNo = p.LedgerPanNo,
                                         BankBranch = p.BankBranch,
                                         Interestmethod = p.Interestmethod,
                                         InterestPtc = p.InterestPtc,
                                         SbuLedgerTypeId = p.SbuLedgerTypeId,

                                         SubledgerDestination = p.SubledgerDestination,
                                         SubledgerClassId = p.SubledgerClassId,
                                         SubledgerContactPerson = p.SubledgerContactPerson,
                                         SubledgerCstNo = p.SubledgerCstNo,
                                         SubledgerCreditDays = p.SubledgerCreditDays,
                                         SubledgerCreditLimit = p.SubledgerCreditLimit,
                                         taxNature = p.taxNature,
                                         Showledger = p.Showledger,
                                         DateOfBirth = p.DateOfBirth,
                                         DateOfMarraige = p.DateOfMarraige,

                                         manualStopBuilding = p.manualStopBuilding,
                                         DeducteeType = p.DeducteeType,
                                         StateId = p.StateId,
                                         SectionId = p.SectionId,
                                         OnlyAccounts = p.OnlyAccounts,


                                         EntryUserId = p.EntryUserId,
                                         EntryUserDate = p.EntryUserDate,
                                         UpdateUserId = p.UpdateUserId,
                                         UpdateUserDate = p.UpdateUserDate

                                     }).ToList();
                        model = query;
                    }
               // }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }


        #endregion LedgerName

        #region SubLedgerClass

        public long AddSubLedgerClass(SubLedgerClass model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    if (context.SubLedgerClass.Any(x => x.SubLedgerClassId == model.SubLedgerClassId))
                        throw new Exception("SubLedgerClassId\"" + model.SubLedgerClassId + "\" is already taken");
                    if (context.SubLedgerClass.Any(x => x.SubLedgerClassName == model.SubLedgerClassName))
                        throw new Exception("SubLedgerClassName\"" + model.SubLedgerClassName + "\" is already taken");

                    context.SubLedgerClass.Add(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return model.SubLedgerClassId;

        }

        public bool DeleteSubLedgerClass(long SubLedgerClassId)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    SubLedgerClass model = context.SubLedgerClass.Find(SubLedgerClassId);
                    context.SubLedgerClass.Remove(model);
                    context.SaveChanges();
                }

            }

            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        public long EditSubLedgerClass(SubLedgerClass model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {

                    /// get the SubLedgerClassGroup

                    var SubLedgerClass = context.SubLedgerClass.Find(model.SubLedgerClassId);


                    SubLedgerClass.CompId = model.CompId;
                    SubLedgerClass.SubLedgerClassId = model.SubLedgerClassId;
                    SubLedgerClass.SubLedgerClassName = model.SubLedgerClassName;
                    SubLedgerClass.UnderId = model.UnderId;
                    SubLedgerClass.IsParent = model.ParentString;
                    SubLedgerClass.IsMain = model.IsMain;
                    SubLedgerClass.UnderLevel = model.UnderLevel;
                    SubLedgerClass.MainId = model.MainId;
                    SubLedgerClass.EntryUserId = model.EntryUserId;
                    SubLedgerClass.EntryUserDate = model.EntryUserDate;
                    SubLedgerClass.UpdateUserId = model.UpdateUserId;
                    SubLedgerClass.UpdateUserDate = model.UpdateUserDate;
                   

                    context.SubLedgerClass.Update(SubLedgerClass);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model.SubLedgerClassId;
        }

        public SubLedgerClass GetSubLedgerClass(long SubLedgerClassId)
        {
            SubLedgerClass model = new SubLedgerClass();
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    model = context.SubLedgerClass.Find(SubLedgerClassId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        //public SubLedgerClassVM GetSubLedgerClassDetails(long SubLedgerClassId)
        //{
        //    SubLedgerClassVM model = new SubLedgerClassVM();
        //    try
        //    {
        //        using (DbApplicationContext context = new DbApplicationContext())
        //        {

        //            var query = (from p in context.SubLedgerClass
        //                         where p.LedgerId == LedgerId
        //                         join q in context.LedgerGroup on p.LedgerGroupId equals q.LedgerGroupId
        //                         select new LedgerNameVM
        //                         {
        //                             LedgerId = p.LedgerId,
        //                             LedgerGroupId = p.LedgerGroupId,
        //                             LedgerGroupName = q.LedgerGroupName,
        //                             LedgerName = p.LedgerName,
        //                             InventoryEffect = p.InventoryEffect,
        //                             CompId = p.CompId,
        //                             IsMain = p.IsMain,
        //                             IsSubledger = p.IsSubledger,
        //                             AgentId = p.AgentId,
        //                             CountryId = p.CountryId,
        //                             Accountent = p.Accountent,
        //                             Ledgeraddress1 = p.Ledgeraddress1,
        //                             Ledgeraddress2 = p.Ledgeraddress2,
        //                             Ledgeraddress3 = p.Ledgeraddress3,
        //                             Ledgerphone1 = p.Ledgerphone1,
        //                             Ledgerphone2 = p.Ledgerphone2,
        //                             Ledgerfax = p.Ledgerfax,
        //                             Ledgeremail = p.Ledgeremail,

        //                             LedgerWebsite = p.LedgerWebsite,
        //                             LedgerVatNo = p.LedgerVatNo,
        //                             LedgerPanNo = p.LedgerPanNo,
        //                             BankBranch = p.BankBranch,
        //                             Interestmethod = p.Interestmethod,
        //                             InterestPtc = p.InterestPtc,
        //                             SbuLedgerTypeId = p.SbuLedgerTypeId,

        //                             SubledgerDestination = p.SubledgerDestination,
        //                             SubledgerClassId = p.SubledgerClassId,
        //                             SubledgerContactPerson = p.SubledgerContactPerson,
        //                             SubledgerCstNo = p.SubledgerCstNo,
        //                             SubledgerCreditDays = p.SubledgerCreditDays,
        //                             SubledgerCreditLimit = p.SubledgerCreditLimit,
        //                             taxNature = p.taxNature,
        //                             Showledger = p.Showledger,
        //                             DateOfBirth = p.DateOfBirth,
        //                             DateOfMarraige = p.DateOfMarraige,

        //                             manualStopBuilding = p.manualStopBuilding,
        //                             DeducteeType = p.DeducteeType,
        //                             StateId = p.StateId,
        //                             SectionId = p.SectionId,
        //                             OnlyAccounts = p.OnlyAccounts,


        //                             EntryUserId = p.EntryUserId,
        //                             EntryUserDate = p.EntryUserDate,
        //                             UpdateUserId = p.UpdateUserId,
        //                             UpdateUserDate = p.UpdateUserDate

        //                         }).FirstOrDefault();
        //            model = query;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return model;
        //}

        public List<SubLedgerClass> SubLedgerClassList(int pageNo, int PageCount)
        {
           // List<LedgerNameVM> model = new List<LedgerNameVM>();
            try
            {

                using (DbApplicationContext context = new DbApplicationContext())
                {
                    return context.SubLedgerClass.OrderBy(t => t.SubLedgerClassName).ToList();
                }

                    //using (DbApplicationContext context = new DbApplicationContext())
                    //{

                    //    var query = (from p in context.Ledger
                    //                     //  where p.LedgerId == LedgerId
                    //                 join q in context.LedgerGroup on p.LedgerGroupId equals q.LedgerGroupId
                    //                 select new LedgerNameVM
                    //                 {
                    //                     LedgerId = p.LedgerId,
                    //                     LedgerGroupId = p.LedgerGroupId,
                    //                     LedgerGroupName = q.LedgerGroupName,
                    //                     LedgerName = p.LedgerName,
                    //                     InventoryEffect = p.InventoryEffect,
                    //                     CompId = p.CompId,
                    //                     IsMain = p.IsMain,
                    //                     IsSubledger = p.IsSubledger,
                    //                     AgentId = p.AgentId,
                    //                     CountryId = p.CountryId,
                    //                     Accountent = p.Accountent,
                    //                     Ledgeraddress1 = p.Ledgeraddress1,
                    //                     Ledgeraddress2 = p.Ledgeraddress2,
                    //                     Ledgeraddress3 = p.Ledgeraddress3,
                    //                     Ledgerphone1 = p.Ledgerphone1,
                    //                     Ledgerphone2 = p.Ledgerphone2,
                    //                     Ledgerfax = p.Ledgerfax,
                    //                     Ledgeremail = p.Ledgeremail,

                    //                     LedgerWebsite = p.LedgerWebsite,
                    //                     LedgerVatNo = p.LedgerVatNo,
                    //                     LedgerPanNo = p.LedgerPanNo,
                    //                     BankBranch = p.BankBranch,
                    //                     Interestmethod = p.Interestmethod,
                    //                     InterestPtc = p.InterestPtc,
                    //                     SbuLedgerTypeId = p.SbuLedgerTypeId,

                    //                     SubledgerDestination = p.SubledgerDestination,
                    //                     SubledgerClassId = p.SubledgerClassId,
                    //                     SubledgerContactPerson = p.SubledgerContactPerson,
                    //                     SubledgerCstNo = p.SubledgerCstNo,
                    //                     SubledgerCreditDays = p.SubledgerCreditDays,
                    //                     SubledgerCreditLimit = p.SubledgerCreditLimit,
                    //                     taxNature = p.taxNature,
                    //                     Showledger = p.Showledger,
                    //                     DateOfBirth = p.DateOfBirth,
                    //                     DateOfMarraige = p.DateOfMarraige,

                    //                     manualStopBuilding = p.manualStopBuilding,
                    //                     DeducteeType = p.DeducteeType,
                    //                     StateId = p.StateId,
                    //                     SectionId = p.SectionId,
                    //                     OnlyAccounts = p.OnlyAccounts,


                    //                     EntryUserId = p.EntryUserId,
                    //                     EntryUserDate = p.EntryUserDate,
                    //                     UpdateUserId = p.UpdateUserId,
                    //                     UpdateUserDate = p.UpdateUserDate

                    //                 }).ToList();
                    //    model = query;
                    //}
                    // }
                }
            catch (Exception ex)
            {
                throw ex;
            }
          //  return model;
        }

        public SubLedgerClassVM GetParentSubLedgerClass(long SubLedgerClassId)
        {
            SubLedgerClassVM model = new SubLedgerClassVM();
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                   

                    var query = (from p in context.SubLedgerClass
                                 where p.SubLedgerClassId == SubLedgerClassId
                                 join q in context.SubLedgerClass on p.UnderId equals q.SubLedgerClassId
                                 select new SubLedgerClassVM
                                 {
                                     SubLedgerClassId = p.SubLedgerClassId,
                                     SubLedgerClassName = p.SubLedgerClassName,
                                     ParentClassName = q.SubLedgerClassName,
                                     UnderId = p.UnderId,
                                     CompId = p.CompId,
                                     IsParent = p.IsParent,
                                     ParentString = p.ParentString,
                                     IsMain = p.IsMain,
                                     UnderLevel = p.UnderLevel,
                                     MainId = p.MainId,                                    
                                     EntryUserId = p.EntryUserId,
                                     EntryUserDate = p.EntryUserDate,
                                     UpdateUserId = p.UpdateUserId,
                                     UpdateUserDate = p.UpdateUserDate

                                 }).FirstOrDefault();
                    model = query;
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        #endregion  SubLedgerClass
    }
}
