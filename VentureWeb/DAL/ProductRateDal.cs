﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VentureWeb.Models;

namespace VentureWeb.DAL
{
    public interface IProductRateaccess
    {
        string AddProductRate(ProductRate model);
        string EditProductRate(ProductRate model);
        List<ProductRate> ProductRateList(int pageNo, int PageCount);
        bool DeleteProductRate(string ProductRateId);
        ProductRate GetProductRate(string productrateId);

    }
    public class ProductRateaccess : IProductRateaccess
    {


        public string AddProductRate(ProductRate model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {


                    if (context.ProductRate.Any(x => x.RateId == model.RateId))
                        throw new Exception("RateId\"" + model.RateId + "\" is already taken");
                    //if (context.ProductRate.Any(x => x.ProductRateName == model.ProductRateName))
                    //    throw new Exception("ProductRateName\"" + model.ProductRateName + "\" is already taken");

                    context.ProductRate.Add(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return model.RateId.ToString();

        }

        public bool DeleteProductRate(string RateId)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    ProductRate model = context.ProductRate.Find(Int32.Parse(RateId));
                    context.ProductRate.Remove(model);
                    context.SaveChanges();



                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        public string EditProductRate(ProductRate model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {

                    /// get the ProductRate

                    var ProductRate = context.ProductRate.Find(model.RateId);

                    ProductRate.RateId = model.RateId;
                    ProductRate.CompId = model.CompId;
                    ProductRate.Rate = model.Rate;
                    ProductRate.EffectDate = model.EffectDate;
                    ProductRate.Status = model.Status;
                    ProductRate.DyeRate = model.DyeRate;
                    ProductRate.TotalRate = model.TotalRate;
                    ProductRate.EntryUserId = model.EntryUserId;
                    ProductRate.EntryUserDate = model.EntryUserDate;
                    ProductRate.UpdateUserId = model.UpdateUserId;
                    ProductRate.UpdateUserDate = model.UpdateUserDate;

                    context.ProductRate.Update(ProductRate);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model.RateId.ToString();
        }

        public ProductRate GetProductRate(string ProductRateId)
        {
            ProductRate model = new ProductRate();
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    model = context.ProductRate.Find(Int32.Parse(ProductRateId));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public List<ProductRate> ProductRateList(int pageNo, int PageCount)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    // return context.ProductRate.Where(i => i.Role == (int)Role.Admin).OrderByDescending(i => i.Id == (int)AssignStatus.NewPatient).ToList();
                    return context.ProductRate.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
