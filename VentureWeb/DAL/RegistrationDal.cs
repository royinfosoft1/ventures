﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VentureWeb.Models;

namespace VentureWeb.DAL
{

    public interface IUseraccess
    {
        string AddUser(Registration model);
        string EditUser(Registration model);
        List<Registration> UserList(int pageNo, int PageCount);
        bool DeleteUser(string empId);
        Registration GetUser(string empId);
        bool ResetPassword(string id, string password);

        List<Registration> GetUserByEmailIdandPassword(string email, string password);

    }
    public class Useraccess : IUseraccess
    {

        public string AddUser(Registration model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {


                    if (context.Registration.Any(x => x.EmailId == model.EmailId))
                        throw new Exception("Emailid\"" + model.EmailId + "\" is already taken");
                    if (context.Registration.Any(x => x.EmpNo == model.EmpNo))
                        throw new Exception("EmpId\"" + model.EmpNo + "\" is already taken");

                    context.Registration.Add(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return model.EmpNo;

        }

        public bool DeleteUser(string empId)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    Registration model = context.Registration.Find(empId);
                    context.Registration.Remove(model);
                    context.SaveChanges();
                   

                   
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        public string EditUser(Registration model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {

                    //get the user
                    var user = context.Registration.Find(model.EmpNo);

                    user.Name = model.Name;
                    user.ReportingTo = model.ReportingTo;
                    user.Designation = model.Designation;
                    user.PhoneNo = model.PhoneNo;
                    user.Password = model.Password;
                    user.UserStatus = model.UserStatus;

                    context.Registration.Update(user);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return model.EmpNo;
        }

        public Registration GetUser(string empId)
        {
            Registration model = new Registration();
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                     model = context.Registration.Find(empId); 
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public List<Registration> UserList(int pageNo, int PageCount)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    // return context.Registration.Where(i => i.Role == (int)Role.Admin).OrderByDescending(i => i.Id == (int)AssignStatus.NewPatient).ToList();
                    return context.Registration.ToList();
                } 
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


        public bool ResetPassword(string id, string password)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    var userdtails = context.Registration.Find(id);
                    userdtails.Password = password;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }


        public List<Registration> GetUserByEmailIdandPassword(string email, string password)
        {
           // Registration model = new Registration();
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                  return context.Registration.Where(s => s.EmailId.Equals(email) && s.Password.Equals(password)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
          //  return model;
        }
    }
}
