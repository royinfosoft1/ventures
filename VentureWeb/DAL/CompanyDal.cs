﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VentureWeb.Models;

namespace VentureWeb.DAL
{

    public interface ICompanyaccess
    {
        string AddCompany(Company model);
        string EditCompany(Company model);
        List<Company> CompanyList(int pageNo, int PageCount);
        bool DeleteCompany(string companyId);
        Company GetCompany(string companyId);

    }
    public class Companyaccess : ICompanyaccess
    {


        public string AddCompany(Company model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {


                    if (context.Company.Any(x => x.CompId == model.CompId))
                        throw new Exception("CompanyId\"" + model.CompId + "\" is already taken");
                    if (context.Company.Any(x => x.CompName == model.CompName))
                        throw new Exception("CompanyName\"" + model.CompName + "\" is already taken");

                    context.Company.Add(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return model.CompId.ToString();

        }

        public bool DeleteCompany(string CompanyId)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    Company model = context.Company.Find(Int32.Parse(CompanyId));
                    context.Company.Remove(model);
                    context.SaveChanges();



                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        public string EditCompany(Company model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {

                    /// get the Company

                    var Company = context.Company.Find(model.CompId);

                    Company.CompId = model.CompId;
                    Company.CompName = model.CompName;
                    Company.CompShortName = model.CompShortName;
                    Company.CompPerAdd1 = model.CompPerAdd1;
                    Company.CompPerAdd2 = model.CompPerAdd2;
                    Company.CompPerAdd3 = model.CompPerAdd3;
                    Company.CompMailAdd1 = model.CompMailAdd1;
                    Company.CompMailAdd2 = model.CompMailAdd2;
                    Company.CompMailAdd3 = model.CompMailAdd3;
                    Company.CompPanNo = model.CompPanNo;
                    Company.CompTanNo = model.CompTanNo;
                    Company.CompListNo = model.CompListNo;
                    Company.CompCstNo = model.CompCstNo;
                    Company.CompVatFlg = model.CompVatFlg;
                    Company.CompBookMstDt = model.CompBookMstDt;
                    Company.CompPhone1 = model.CompPhone1;
                    Company.CompPhone2 = model.CompPhone2;
                    Company.CompPhone3 = model.CompPhone3;
                    Company.CompPhone4 = model.CompPhone4;
                    Company.CompFaxNo1 = model.CompFaxNo1;
                    Company.CompFaxNo2 = model.CompFaxNo2;
                    Company.CompEmailId1 = model.CompEmailId1;
                    Company.CompEmailId2 = model.CompEmailId2;
                    Company.CompWebsite = model.CompWebsite;
                    Company.CompStatus = model.CompStatus;
                    Company.EntryUserId = model.EntryUserId;
                    Company.EntryUserDate = model.EntryUserDate;
                    Company.UpdateUserId = model.UpdateUserId;
                    Company.UpdateUserDate = model.UpdateUserDate;
                    

                    context.Company.Update(Company);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model.CompId.ToString();
        }

        public Company GetCompany(string CompId)
        {
            Company model = new Company();
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    model = context.Company.Find(Int32.Parse(CompId));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public List<Company> CompanyList(int pageNo, int PageCount)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    // return context.Company.Where(i => i.Role == (int)Role.Admin).OrderByDescending(i => i.Id == (int)AssignStatus.NewPatient).ToList();
                    return context.Company.OrderBy(t => t.CompName).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

