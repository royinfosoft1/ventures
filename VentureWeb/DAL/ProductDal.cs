﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VentureWeb.Models;
using VentureWeb.ViewModel;

namespace VentureWeb.DAL
{
    public interface IProductaccess
    {
        long AddProduct(Product model);
        long EditProduct(Product model);
        List<ProductViewModel> ProductList(int pageNo, int PageCount);
        bool DeleteProduct(long empId);
        Product GetProduct(long empId);
        bool SetFeaturedProduct(long ProductId, bool isfeatured);
        List<ProductViewModel> FeaturedProductList(int pageNo, int PageCount);
        List<ProductViewModel> GetProductListByCategoryId(long CategoryId);

        List<ImageDetails> GetProductImageListByProductIdAndImageDesc(long ProductId, string ImageDesc);
        List<ProductViewModel> GetProductListByCategoryIdAndProductName(long CategoryId, long ClassId, string ProductName);

    }
    public class Productaccess : IProductaccess
    {
        public long AddProduct(Product model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {


                    if (context.Product.Any(x => x.ProductId == model.ProductId))
                        throw new Exception("ProductId\"" + model.ProductId + "\" is already taken");
                    if (context.Product.Any(x => x.ProductName == model.ProductName))
                        throw new Exception("ProductName\"" + model.ProductName + "\" is already taken");

                    context.Product.Add(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return model.ProductId;

        }

        public bool DeleteProduct(long ProductId)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    Product model = context.Product.Find(ProductId);
                    context.Product.Remove(model);
                    context.SaveChanges();



                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        public long EditProduct(Product model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {

                    /// get the Product

                    var Product = context.Product.Find(model.ProductId);

                    Product.ProductName = model.ProductName;
                    Product.CompId = model.CompId;
                    Product.ProductClassId = model.ProductClassId;
                    Product.ProductCategoryId = model.ProductCategoryId;
                    Product.ProductName = model.ProductName;
                    Product.Width = model.Width;
                    Product.Width1 = model.Width1;
                    Product.Width2 = model.Width2;
                    Product.Width3 = model.Width3;
                    Product.WidthTotal = model.WidthTotal;
                    Product.ReferenceCode = model.ReferenceCode;
                    Product.ValidationMethod = model.ValidationMethod;
                    Product.OpeningUnitId = model.OpeningUnitId;
                    Product.AlertUnitId = model.AlertUnitId;
                    Product.AlertUnitMethod = model.AlertUnitMethod;
                    Product.AlertUnitFactor = model.AlertUnitFactor;
                    Product.SaleUnitId = model.SaleUnitId;
                    Product.FreeQuantityTag = model.FreeQuantityTag;
                    Product.MinimumLevel = model.MinimumLevel;
                    Product.MaximunLevel = model.MaximunLevel;
                    Product.ReorderLevel = model.ReorderLevel;
                    Product.DefaultPurchaseRate = model.DefaultPurchaseRate;
                    Product.DefaultSateRate = model.DefaultSateRate;
                    Product.MinimumSaleRate = model.MinimumSaleRate;
                    Product.MaximumSaleRate = model.MaximumSaleRate;
                    Product.TaxPercent = model.TaxPercent;
                    Product.IsFinishedProduct = model.IsFinishedProduct;
                    Product.EntryUserId = model.EntryUserId;
                    Product.EntryUserDate = model.EntryUserDate;
                    Product.UpdateUserId = model.UpdateUserId;
                    Product.UpdateUserDate = model.UpdateUserDate;

                    context.Product.Update(Product);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model.ProductId;
        }

        public Product GetProduct(long ProductId)
        {
            Product model = new Product();
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    model = context.Product.Find(ProductId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        //public List<Product> ProductList(int pageNo, int PageCount)
        public List<ProductViewModel> ProductList(int pageNo, int PageCount)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    // return context.Product.Where(i => i.Role == (int)Role.Admin).OrderByDescending(i => i.Id == (int)AssignStatus.NewPatient).ToList();
                    var productlist = (from p in context.Product
                                       select new ProductViewModel
                                       {
                                           //DefaultImagePath = i.ImagePath,
                                           ProductId = p.ProductId,
                                           ProductName = p.ProductName,
                                           //ProductCategory = c.ProductCategoryName,
                                           //ProductCategoryId = c.ProductCategoryId,
                                           IsFeatured = p.IsFeatured

                                       }).ToList();


                    return productlist;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SetFeaturedProduct(long ProductId, bool isfeatured)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    var productdtails = context.Product.Find(ProductId);
                    productdtails.IsFeatured = isfeatured;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        public List<ProductViewModel> FeaturedProductList(int pageNo, int PageCount)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    //  return context.Product.Where(i => i.IsFeatured == true).ToList();
                    // return context.Product.ToList();

                    var productlist =
                            (from p in context.Product
                             join i in context.ImageDetails on p.ProductId equals i.LinkId
                             where p.IsFeatured == true && i.IsDefault == true
                             select new ProductViewModel
                             {

                                 DefaultImagePath = i.ImagePath,
                                 ProductId = p.ProductId,
                                 ProductName = p.ProductName,


                             }).ToList();


                    return productlist;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<ProductViewModel> GetProductListByCategoryId(long CategoryId)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {

                    var productlist =
                            (from p in context.Product
                             join i in context.ImageDetails on p.ProductId equals i.LinkId
                             join c in context.ProductCategory on p.ProductCategoryId equals c.ProductCategoryId
                             where i.IsDefault == true && p.ProductCategoryId == Convert.ToInt32(CategoryId)
                             select new ProductViewModel
                             {

                                 DefaultImagePath = i.ImagePath,
                                 ProductId = p.ProductId,
                                 ProductName = p.ProductName,


                             }).ToList();


                    return productlist;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<ImageDetails> GetProductImageListByProductIdAndImageDesc(long ProductId, string ImageDesc)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {

                    return context.ImageDetails.Where(i => i.ImageType == "P" && i.ImageDesc == ImageDesc
                              && i.LinkId == Convert.ToInt32(ProductId)).ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProductViewModel> GetProductListByCategoryIdAndProductName(long CategoryId, long ClassId, string ProductName)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {

                    var productlist =
                            (from p in context.Product
                             join i in context.ImageDetails on p.ProductId equals i.LinkId where i.IsDefault == true
                             join c in context.ProductCategory on p.ProductCategoryId equals c.ProductCategoryId
                             join pc in context.ProductClass on p.ProductClassId equals pc.ProductClassId
                            
                            // 
                             select new ProductViewModel
                             {
                                 DefaultImagePath = i.ImagePath,
                                 ProductId = p.ProductId,
                                 ProductName = p.ProductName,
                                 ProductCategory = c.ProductCategoryName,
                                 ProductCategoryId = c.ProductCategoryId,
                                 IsFeatured = p.IsFeatured,
                                 ProductClass=pc.ProductClassName
                             }).AsEnumerable();

                    //if (!String.IsNullOrEmpty(CategoryId))
                    if (CategoryId!=0)
                    {
                        productlist = (from x in productlist
                                       where x.ProductCategoryId == Convert.ToInt32(CategoryId)
                                       select x
                                       ).AsEnumerable();
                    }
                    //else if (!String.IsNullOrEmpty(ClassId))
                    if (ClassId != 0)
                    {
                        productlist = (from x in productlist
                                       where x.ProductClassId == Convert.ToInt32(ClassId)
                                       select x
                                       ).AsEnumerable();
                    }

                    if (!String.IsNullOrEmpty(ProductName))
                    {
                        productlist = (from x in productlist
                                           // where x.ProductName == ProductName
                                       where x.ProductName.Contains(ProductName)
                                       select x
                                       ).AsEnumerable();
                    }
                    return productlist.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
