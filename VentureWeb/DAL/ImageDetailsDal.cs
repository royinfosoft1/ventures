﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VentureWeb.Models;

namespace VentureWeb.DAL
{

    public interface IImageDetailsaccess
    {
        string AddImageDetails(ImageDetails model);
        string EditImageDetails(ImageDetails model);
        List<ImageDetails> ImageDetailsList(int pageNo, int PageCount);
        bool DeleteImageDetails(string ImageId);
        bool SetDefaultImage(long ImageId,  bool isdefault);
        bool SetDefaultImageStateByProductId(long productid, bool isdefault);
        ImageDetails GetImageDetails(string ImageId);
        List<ImageDetails> GetImageDetailsbyLinkIdAndImageType(string LinkId, string ImageType);
        List<ImageDetails> GetImageDetailsbyLinkIdAndImageTypeAndImageDesc(string LinkId, string ImageType,string ImageDesc);

    }
    public class ImageDetailsaccess : IImageDetailsaccess
    {

        public string AddImageDetails(ImageDetails model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {


                    if (context.ImageDetails.Any(x => x.ImageId == model.ImageId))
                        throw new Exception("ImageId\"" + model.ImageId + "\" is already taken");
                    if (context.ImageDetails.Any(x => x.ImageName == model.ImageName))
                        throw new Exception("ImageName\"" + model.ImageName + "\" is already taken");

                    context.ImageDetails.Add(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return model.ImageId.ToString();

        }

        public bool DeleteImageDetails(string ImageId)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    ImageDetails model = context.ImageDetails.Find(Int32.Parse(ImageId));
                    context.ImageDetails.Remove(model);
                    context.SaveChanges();



                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        public bool SetDefaultImage(long ImageId, bool isdefault)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    var imagedtails = context.ImageDetails.Find(ImageId);
                    imagedtails.IsDefault = isdefault;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }
        public bool SetDefaultImageStateByProductId(long productid, bool isdefault)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                     context.ImageDetails.Where(i => i.LinkId == productid && i.ImageType == "P").ToList().ForEach(x => x.IsDefault = false);
                     context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        public string EditImageDetails(ImageDetails model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {

                    /// get the ImageDetails

                    var imagedtails = context.ImageDetails.Find(model.ImageId);

                    imagedtails.ImageId = model.ImageId;
                    imagedtails.ImageType = model.ImageType;
                    imagedtails.LinkId = model.LinkId;
                    imagedtails.ImagePath = model.ImagePath;
                    imagedtails.ImageName = model.ImageName;
                    imagedtails.ImageExt = model.ImageExt;
                    imagedtails.ImageDesc = model.ImageDesc;
                    imagedtails.IsDefault = model.IsDefault;
                   

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model.ImageId.ToString();
        }

        public ImageDetails GetImageDetails(string ImageId)
        {
            ImageDetails model = new ImageDetails();
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    model = context.ImageDetails.Find(Int32.Parse(ImageId));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public List<ImageDetails> GetImageDetailsbyLinkIdAndImageType(string LinkId,string ImageType)
        {
          //  ImageDetails model = new ImageDetails();
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    // model = context.ImageDetails.Find(Int32.Parse(LinkId));
                    return context.ImageDetails.Where(i => i.LinkId == Convert.ToInt32(LinkId) && i.ImageType==ImageType).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
          //  return model;
        }
        public List<ImageDetails> GetImageDetailsbyLinkIdAndImageTypeAndImageDesc(string LinkId, string ImageType, string ImageDesc) 
        {
            //  ImageDetails model = new ImageDetails();
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    // model = context.ImageDetails.Find(Int32.Parse(LinkId));
                    return context.ImageDetails.Where(i => i.LinkId == Convert.ToInt32(LinkId) && i.ImageType == ImageType && i.ImageDesc==ImageDesc).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ImageDetails> ImageDetailsList(int pageNo, int PageCount)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    // return context.ImageDetails.Where(i => i.Role == (int)Role.Admin).OrderByDescending(i => i.Id == (int)AssignStatus.NewPatient).ToList();
                    return context.ImageDetails.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
