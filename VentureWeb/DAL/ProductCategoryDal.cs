﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VentureWeb.Models;

namespace VentureWeb.DAL
{

    public interface IProductCategoryaccess
    {
        string AddProductCategory(ProductCategory model);
        string EditProductCategory(ProductCategory model);
        List<ProductCategory> ProductCategoryList(int pageNo, int PageCount);
        bool DeleteProductCategory(long empId);
        ProductCategory GetProductCategory(string empId);

    }
    public class ProductCategoryaccess : IProductCategoryaccess
    {


        public string AddProductCategory(ProductCategory model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {


                    if (context.ProductCategory.Any(x => x.ProductCategoryId == model.ProductCategoryId))
                        throw new Exception("ProductCategoryId\"" + model.ProductCategoryId + "\" is already taken");
                    if (context.ProductCategory.Any(x => x.ProductCategoryName == model.ProductCategoryName))
                        throw new Exception("ProductCategoryName\"" + model.ProductCategoryName + "\" is already taken");

                    context.ProductCategory.Add(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return model.ProductCategoryId.ToString();

        }

        public bool DeleteProductCategory(long productCategoryId)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    //check if used

                    if (context.Product.Any(t => t.ProductCategoryId == productCategoryId))
                    {
                        return false;
                    }
                    else
                    {
                        //then delete
                        ProductCategory model = context.ProductCategory.Find(productCategoryId);
                        context.ProductCategory.Remove(model);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        public string EditProductCategory(ProductCategory model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {

                    /// get the ProductCategory

                    var ProductCategory = context.ProductCategory.Find(model.ProductCategoryId);

                    ProductCategory.ProductCategoryId = model.ProductCategoryId;
                    ProductCategory.ProductCategoryName = model.ProductCategoryName;

                    context.ProductCategory.Update(ProductCategory);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return model.ProductCategoryId.ToString();
        }

        public ProductCategory GetProductCategory(string ProductCategoryId)
        {
            ProductCategory model = new ProductCategory();
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    model = context.ProductCategory.Find(Convert.ToInt32(ProductCategoryId));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public List<ProductCategory> ProductCategoryList(int pageNo, int PageCount)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    // return context.ProductCategory.Where(i => i.Role == (int)Role.Admin).OrderByDescending(i => i.Id == (int)AssignStatus.NewPatient).ToList();
                    return context.ProductCategory.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
