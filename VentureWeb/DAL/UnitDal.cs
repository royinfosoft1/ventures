﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VentureWeb.Models;

namespace VentureWeb.DAL
{

    public interface IUnitaccess
    {
        string AddUnit(Unit model);
        string EditUnit(Unit model);
        List<Unit> UnitList(int pageNo, int PageCount);
        bool DeleteUnit(string unitId);
        Unit GetUnit(string unitId);

    }
    public class Unitaccess : IUnitaccess
    {


        public string AddUnit(Unit model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {


                    if (context.Unit.Any(x => x.UnitId == model.UnitId))
                        throw new Exception("UnitId\"" + model.UnitId + "\" is already taken");
                    if (context.Unit.Any(x => x.UnitName == model.UnitName))
                        throw new Exception("UnitName\"" + model.UnitName + "\" is already taken");

                    context.Unit.Add(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return model.UnitId.ToString();

        }

        public bool DeleteUnit(string UnitId)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    Unit model = context.Unit.Find(Int32.Parse(UnitId));
                    context.Unit.Remove(model);
                    context.SaveChanges();



                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        public string EditUnit(Unit model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {

                    /// get the Unit

                    var unit = context.Unit.Find(model.UnitId);

                    unit.UnitId = model.UnitId;
                    unit.CompId = model.CompId;
                    unit.UnitName = model.UnitName;
                    unit.UnitDescription = model.UnitDescription;
                    unit.UnitDecimalPlace = model.UnitDecimalPlace;
                    unit.EntryUserId = model.EntryUserId;
                    unit.EntryUserDate = model.EntryUserDate;
                    unit.UpdateUserId = model.UpdateUserId;
                    unit.UpdateUserDate = model.UpdateUserDate;

                    context.Unit.Update(unit);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model.UnitId.ToString();
        }

        public Unit GetUnit(string UnitId)
        {
            Unit model = new Unit();
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    model = context.Unit.Find(Int32.Parse(UnitId));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public List<Unit> UnitList(int pageNo, int PageCount)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    // return context.Unit.Where(i => i.Role == (int)Role.Admin).OrderByDescending(i => i.Id == (int)AssignStatus.NewPatient).ToList();
                    return context.Unit.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

