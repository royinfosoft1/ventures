﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VentureWeb.Models;

namespace VentureWeb.DAL
{

    public interface IProductClassAccess
    {
        long AddProductClass(ProductClass model);
        long EditProductClass(ProductClass model);
        List<ProductClass> ProductClassList(int pageNo, int PageCount);
        bool DeleteProductClass(long productclassId);
        ProductClass GetProductClass(string productclassId);

    }
    public class ProductClassAccess : IProductClassAccess
    {


        public long AddProductClass(ProductClass model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {


                    if (context.ProductClass.Any(x => x.ProductClassId == model.ProductClassId))
                        throw new Exception("ProductClassId\"" + model.ProductClassId + "\" is already taken");
                    if (context.ProductClass.Any(x => x.ProductClassName == model.ProductClassName))
                        throw new Exception("ProductClassName\"" + model.ProductClassName + "\" is already taken");

                    context.ProductClass.Add(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return model.ProductClassId;

        }

        public bool DeleteProductClass(long ProductClassId)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {

                    //check if used

                    if (context.Product.Any(t => t.ProductClassId == ProductClassId))
                    {
                        return false;
                    }
                    else
                    {
                        //then delete

                        ProductClass model = context.ProductClass.Find(ProductClassId);
                    context.ProductClass.Remove(model);
                    context.SaveChanges();

                    }
                }

            }
            
            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        public long EditProductClass(ProductClass model)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {

                    /// get the ProductClass

                    var ProductClass = context.ProductClass.Find(model.ProductClassId);

                    ProductClass.ProductClassId = model.ProductClassId;
                    ProductClass.CompId = model.CompId;
                    ProductClass.ProductClassName = model.ProductClassName;
                    ProductClass.UnderId = model.UnderId;
                    ProductClass.IsParent = model.IsParent;
                    ProductClass.ParentString = model.ParentString;
                    ProductClass.IsMain = model.IsMain;
                    ProductClass.EntryUserId = model.EntryUserId;
                    ProductClass.EntryUserDate = model.EntryUserDate;
                    ProductClass.UpdateUserId = model.UpdateUserId;
                    ProductClass.UpdateUserDate = model.UpdateUserDate;

                    context.ProductClass.Update(ProductClass);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model.ProductClassId;
        }

        public ProductClass GetProductClass(string ProductClassId)
        {
            ProductClass model = new ProductClass();
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    model = context.ProductClass.Find(Int32.Parse(ProductClassId));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public List<ProductClass> ProductClassList(int pageNo, int PageCount)
        {
            try
            {
                using (DbApplicationContext context = new DbApplicationContext())
                {
                    // return context.ProductClass.Where(i => i.Role == (int)Role.Admin).OrderByDescending(i => i.Id == (int)AssignStatus.NewPatient).ToList();
                    return context.ProductClass.OrderBy(t=>t.ProductClassName).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

