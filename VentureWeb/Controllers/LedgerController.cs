﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VentureWeb.DAL;
using VentureWeb.Models;
using VentureWeb.ViewModel;

namespace VentureWeb.Controllers
{
    public class LedgerController : Controller
    {
        private readonly ILogger<LedgerController> _logger;
        private readonly ILedgerAccess _ledgeraccess;
        public LedgerController(ILogger<LedgerController> logger, ILedgerAccess ledgeraccess)
        {
            _logger = logger;
            _ledgeraccess = ledgeraccess;
        }

        #region LedgerGroup
        


        [HttpGet]
        public IActionResult AddEditLedgerGroup(long id,long underid)
        {
            try
            {
                if (id==0)
                {
                    TempData["Mode"] = "Add";
                    var ledgergroup = _ledgeraccess.GetParentLedgerGroup(underid);
                    var addledgervm = new LedgerGroupVM();
                    addledgervm.ParentGroupName = ledgergroup.LedgerGroupName;
                    addledgervm.UnderId = ledgergroup.LedgerGroupId;
                    return View(addledgervm);
                }
                else
                {
                    TempData["Mode"] = "Edit";
                    var ledgergroup = _ledgeraccess.GetLedgerGroup(id);
                    return View(ledgergroup);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public IActionResult LedgerGroup(LedgerGroup model, bool add)
        {

            try
            {
                model.EntryUserDate = DateTime.Now;
                model.UpdateUserDate = DateTime.Now;
                model.EntryUserId = Convert.ToInt32(HttpContext.Session.Keys.Select(x => x == "userId").FirstOrDefault());
                model.CompId = 1;
                if (add)
                {
                    if (model.LedgerGroupId == null)
                        throw new Exception("Enter LedgerGroup Id.");
                    if (model.LedgerGroupName == null)
                        throw new Exception("Enter LedgerGroup Name.");
                    
                    _ledgeraccess.AddLedgerGroup(model);
                    TempData["alertMessage"] = "ledgergroup added Successfully";
                }
                else
                {
                    //model.EntryUserDate = DateTime.Now;
                    //model.UpdateUserDate = DateTime.Now;
                    _ledgeraccess.EditLedgerGroup(model);
                    TempData["alertMessage"] = "LedgerGroup Updated Successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ListLedgerGroup");
        }

        [HttpGet]
        public IActionResult ListLedgerGroup()
        {
            try
            {
                var ledgergroupList = _ledgeraccess.LedgerGroupList(1, 10);
                return View(ledgergroupList);
            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpGet]
        public IActionResult Delete(long id)
        {
            try
            {
                if (id != 0)
                {

                    bool flag = _ledgeraccess.DeleteLedgerGroup(id);
                    TempData["alertMessage"] = "Deleted Successfully";

                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
                        
            return RedirectToAction("ListLedgerGroup");
        }


        #endregion LedgerGroup


        #region LedgerName

        [HttpGet]
        public IActionResult AddEditLedgerName(long id)
        {
            try
            {
                var AddEditLedgerNameVM = new AddEditLedgerNameVM();
                AddEditLedgerNameVM.LedgerGroup = _ledgeraccess.LedgerGroupList(1,20);
                if (id==0)
                {
                    TempData["Mode"] = "Add";
                    return View(AddEditLedgerNameVM);
                }
                else
                {
                    TempData["Mode"] = "Edit";
                    AddEditLedgerNameVM.Ledger = _ledgeraccess.GetLedgerDetails(id);
                    return View(AddEditLedgerNameVM);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public IActionResult LedgerName(Ledger model, bool add)
        {

            try
            {
                model.EntryUserDate = DateTime.Now;
                model.UpdateUserDate = DateTime.Now;
                model.EntryUserId = Convert.ToInt32(HttpContext.Session.Keys.Select(x => x == "userId").FirstOrDefault());
                model.CompId = 1;
                if (add)
                {
                    if (model.LedgerId == null)
                        throw new Exception("Enter Ledger Id.");
                    if (model.LedgerName == null)
                        throw new Exception("Enter Ledger Name.");
                    _ledgeraccess.AddLedgerName(model);
                    TempData["alertMessage"] = "ledgername added Successfully";
                }
                else
                {
                    _ledgeraccess.EditLedgerName(model);

                    TempData["alertMessage"] = "LedgerName Updated Successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ListLedgerName");
        }

        [HttpGet]
        public IActionResult ListLedgerName()
        {
            try
            {
                var ledgernameList = _ledgeraccess.LedgerNameList(1, 10);
                return View(ledgernameList);
            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpGet]
        public IActionResult DeleteLedgerName(long id)
        {
            try
            {
                if (id != 0)
                {

                    bool flag = _ledgeraccess.DeleteLedgerName(id);
                    TempData["alertMessage"] = "Deleted Successfully";

                }


            }
            catch (Exception ex)
            {

                throw ex;
            }

            
            return RedirectToAction("ListLedgerName");
        }


        #endregion LedgerName

        #region SubLedgerClass

        [HttpGet]
        public IActionResult AddEditSubLedgerClass(long id, long underid)
        {
            try
            {
                if (id == 0)
                {
                    TempData["Mode"] = "Add";
                    var subledgerclass = _ledgeraccess.GetParentSubLedgerClass(underid);
                    var addsubledgerclassvm = new SubLedgerClassVM();
                    addsubledgerclassvm.ParentClassName = subledgerclass.SubLedgerClassName;
                    addsubledgerclassvm.UnderId = subledgerclass.SubLedgerClassId;
                    return View(addsubledgerclassvm);
                }
                else
                {
                    TempData["Mode"] = "Edit";
                    var addsubledgerclassvm = _ledgeraccess.GetParentSubLedgerClass(id);
                    return View(addsubledgerclassvm);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public IActionResult SubLedgerClass(SubLedgerClass model, bool add)
        {

            try
            {
                model.EntryUserDate = DateTime.Now;
                model.UpdateUserDate = DateTime.Now;
                model.EntryUserId = Convert.ToInt32(HttpContext.Session.Keys.Select(x => x == "userId").FirstOrDefault());
                model.CompId = 1;
                if (add)
                {
                    if (model.SubLedgerClassId == null)
                        throw new Exception("Enter SubLedgerClass Id.");
                    if (model.SubLedgerClassName == null)
                        throw new Exception("Enter SubLedgerClass Name.");

                    _ledgeraccess.AddSubLedgerClass(model);
                    TempData["alertMessage"] = "subledgerclass added Successfully";
                }
                else
                {
                    //model.EntryUserDate = DateTime.Now;
                    //model.UpdateUserDate = DateTime.Now;
                    _ledgeraccess.EditSubLedgerClass(model);
                    TempData["alertMessage"] = "SubLedgerClass Updated Successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ListSubLedgerClass");
        }

        [HttpGet]
        public IActionResult ListSubLedgerClass()
        {
            try
            {
                var subledgerclassList = _ledgeraccess.SubLedgerClassList(1, 10);
                return View(subledgerclassList);
            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpGet]
        public IActionResult DeleteSubLedgerClass(long id)
        {
            try
            {
                if (id != 0)
                {

                    bool flag = _ledgeraccess.DeleteSubLedgerClass(id);
                    TempData["alertMessage"] = "Deleted Successfully";

                }


            }
            catch (Exception ex)
            {

                throw ex;
            }

            return RedirectToAction("ListSubLedgerClass");
        }

        #endregion SubLedgerClass

    }
}
