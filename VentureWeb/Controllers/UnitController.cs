﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VentureWeb.DAL;
using VentureWeb.Models;

namespace VentureWeb.Controllers
{
    public class UnitController : Controller
    {
        private readonly ILogger<UnitController> _logger;
        private readonly IUnitaccess _unitaccess;
        public UnitController(ILogger<UnitController> logger, IUnitaccess unitaccess)
        {
            _logger = logger;
            _unitaccess = unitaccess;
        }

        [HttpGet]
        public IActionResult AddEditUnit(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                TempData["Mode"] = "Add";
                return View();
            }
            else
            {
                TempData["Mode"] = "Edit";
                var unit = _unitaccess.GetUnit(id);
                return View(unit);
            }
        }

        [HttpPost]
        public IActionResult Unit(Unit model, bool add)
        {

            try
            {
                model.EntryUserDate = DateTime.Now;
                model.UpdateUserDate = DateTime.Now;
                model.EntryUserId = Convert.ToInt32(HttpContext.Session.Keys.Select(x => x == "userId").FirstOrDefault());

                if (add)
                {
                    if (model.UnitId == null)
                        throw new Exception("Enter Unit Id.");
                    if (model.UnitName == null)
                        throw new Exception("Enter Unit Name.");
                    _unitaccess.AddUnit(model);
                    TempData["alertMessage"] = "unit added Successfully";
                }
                else
                {
                    _unitaccess.EditUnit(model);

                    TempData["alertMessage"] = "Unit Updated Successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ListUnit");
        }

        [HttpGet]
        public IActionResult ListUnit()

        {
            try
            {
                var unitList = _unitaccess.UnitList(1, 10);
                return View(unitList);
            }
            catch (Exception)
            {

                throw;
            }

        }


        [HttpGet]
        public IActionResult Delete(string id)
        {
            try
            {
                if (id != "")
                {

                    bool flag = _unitaccess.DeleteUnit(id);
                    TempData["alertMessage"] = "Deleted Successfully";

                }


            }
            catch (Exception ex)
            {

                throw ex;
            }

            //  return View(personalDetail);
            return RedirectToAction("ListUnit");
        }

    }
}
