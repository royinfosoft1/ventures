﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VentureWeb.DAL;
using VentureWeb.Models;
using VentureWeb.ViewModel;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Http.Extensions;


namespace VentureWeb.Controllers
{
    public class ProductController : Controller
    {
        private readonly ILogger<ProductController> _logger;
        private readonly IProductaccess _productaccess;
        private readonly IProductClassAccess _productclassaccess;
        private readonly IProductCategoryaccess _productcategoryaccess;
        private readonly IUnitaccess _unitaccess;
        private readonly IImageDetailsaccess _imagedetailsaccess;
        public ProductController(ILogger<ProductController> logger,
            IProductaccess productaccess, IProductClassAccess productclassaccess,
            IProductCategoryaccess productcategoryaccess, IUnitaccess unitaccess, IImageDetailsaccess imagedetailsaccess)
        {
            _logger = logger;
            _productaccess = productaccess;
            _productclassaccess = productclassaccess;
            _productcategoryaccess = productcategoryaccess;
            _unitaccess = unitaccess;
            _imagedetailsaccess = imagedetailsaccess;
        }

        [HttpGet]
        public IActionResult AddEditProduct(string id)
        {
            var AddEditProductViewModel = new AddEditProductViewModel();
            AddEditProductViewModel.ProductClass = _productclassaccess.ProductClassList(1, 20);
            AddEditProductViewModel.ProductCategory = _productcategoryaccess.ProductCategoryList(1, 20);
            AddEditProductViewModel.Unit = _unitaccess.UnitList(1, 20);
          //  AddEditProductViewModel.UnitMethod = GetUnitMethodDetails();
            AddEditProductViewModel.ProductImages = _imagedetailsaccess.GetImageDetailsbyLinkIdAndImageTypeAndImageDesc(id, "P", "P"); /// P-Product, P-Product Picture
            AddEditProductViewModel.WcareImages = _imagedetailsaccess.GetImageDetailsbyLinkIdAndImageTypeAndImageDesc(id, "P", "W"); /// P-Product, W-Wcare


            if (String.IsNullOrEmpty(id))
            {
                TempData["Mode"] = "Add";
                AddEditProductViewModel.Product = null;
                
            }
            else
            {
                TempData["Mode"] = "Edit";
                AddEditProductViewModel.Product = _productaccess.GetProduct(Convert.ToInt64(id));
            }
            return View(AddEditProductViewModel);
        }

        [HttpPost]
        public IActionResult AddEditProduct(Product model, bool add, List<IFormFile> files, List<IFormFile> filesW)
        {
            try
            {
                if (add)
                {
                    if (model.ProductId == null)
                        throw new Exception("Enter Product Id.");
                    if (model.ProductName == null)
                        throw new Exception("Enter Product Name.");
                    var newProductid=_productaccess.AddProduct(model);
                    SaveImages(files, model.ProductId.ToString(), "P");
                    SaveImages(filesW, model.ProductId.ToString(), "W");
                    TempData["alertMessage"] = "Product added Successfully";
                    return RedirectToAction("AddEditProduct", new { id=newProductid });
                }
                else
                {
                    _productaccess.EditProduct(model);
                    SaveImages(files, model.ProductId.ToString(), "P");
                    SaveImages(filesW, model.ProductId.ToString(), "W");
                    TempData["alertMessage"] = "Product Updated Successfully";
                    return RedirectToAction("ProductList");
                }
            }
            catch (Exception ex)
            {
              //  throw ex;
                TempData["alertMessage"] = ex.Message;
                return RedirectToAction("AddEditProduct");
            }
            
        }

        public void SaveImages(List<IFormFile> files, string productid, string imagedesc)
        {
            ImageDetails imageDetails = new ImageDetails();
            var fileName = "";
            var myUniqueFileName = "";
            var fileExtension = "";
            var newFileName = "";
            var filepath = "";

            if (files != null)
            {
                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {
                        //Getting FileName
                        fileName = Path.GetFileName(file.FileName);

                        //Assigning Unique Filename (Guid)
                        myUniqueFileName = Convert.ToString(Guid.NewGuid());

                        //Getting file Extension
                        fileExtension = Path.GetExtension(fileName);

                        // concatenating  FileName + FileExtension
                        newFileName = String.Concat(myUniqueFileName, fileExtension);

                        // Combines two strings into a path.
                        var filepathtemp = Directory.GetCurrentDirectory() + "\\wwwroot\\Images\\ProductImages\\" + productid;// + $@"\{newFileName}";
                        bool exists = System.IO.Directory.Exists(filepathtemp);

                        if (!exists)
                            System.IO.Directory.CreateDirectory(filepathtemp);
                        //filepath =  new Microsoft.Extensions.FileProviders.PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Images/ProductImages/"+productid)).Root + $@"\{newFileName}";
                        filepath = new Microsoft.Extensions.FileProviders.PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Images", "ProductImages", productid)).Root + $@"{newFileName}";

                        // string subPath = "ImagesPath"; // Your code goes here



                        using (FileStream fs = System.IO.File.Create(filepath))
                        // using (FileStream fs = System.IO.File.Create(filepath))
                        // using (var fs = System.IO.Directory.CreateDirectory(filepath))
                        {
                            imageDetails.ImageId = 0;
                            imageDetails.LinkId = Convert.ToInt32(productid);
                            imageDetails.ImageType = "P";
                            imageDetails.ImagePath = filepath;
                            imageDetails.ImageName = newFileName;
                            imageDetails.ImageDesc = imagedesc;
                            imageDetails.ImageExt = fileExtension;

                            _imagedetailsaccess.AddImageDetails(imageDetails);
                            file.CopyTo(fs);
                            fs.Flush();

                        }

                    }
                }
            }
            // return View();
        }

        [HttpGet]
        [ActionName("ProductList")]
        public IActionResult ProductList(long ddlProductCategory, long ddlProductClass, string ProductName)
        {
            try
            {

                var ListProductViewModel = new ListProductViewModel();

                ListProductViewModel.ProductClassList = _productclassaccess.ProductClassList(1, 20);
                ListProductViewModel.ProductCategoryList = _productcategoryaccess.ProductCategoryList(1, 20);
                ListProductViewModel.productList = _productaccess.GetProductListByCategoryIdAndProductName(ddlProductCategory, ddlProductClass, ProductName);
              
                if (ListProductViewModel.productList.Count == 0)
                {
                    TempData["alertMessage"] = "No data found.";
                }
                return View("ListProduct", ListProductViewModel);
            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpGet]
        public IActionResult Delete(long id)
        {
            try
            {
                if (id != 0)
                {
                    bool flag = _productaccess.DeleteProduct(id);
                    TempData["alertMessage"] = "Deleted Successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ListProduct");
        }

        [HttpGet]
        public JsonResult DeleteProductImages(string id, string productid)
        {
            var message = new Message();
            try
            {
                if (id != "")
                {
                    var imagedetails = _imagedetailsaccess.GetImageDetails(id);
                    bool flag = _imagedetailsaccess.DeleteImageDetails(id);

                    var path = imagedetails.ImagePath;//Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\image\\Programcilar", "controlller.jpg");

                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    message.status = "s";
                    TempData["alertMessage"] = "Deleted Successfully";
                }
            }
            catch (Exception ex)
            {
                message.status = "e";
                message.body = ex.Message;
                throw ex;
            }

            // return RedirectToAction( "AddEditProduct", "Product", new { Id = productid });
            return Json(message);
            ;
        }

        [HttpGet]
        public JsonResult SetDefaultProductPicture(long id, long productid, bool isdefault)
        {
            var message = new Message();
            try
            {
                if (id != 0)
                {
                    bool setfalse = _imagedetailsaccess.SetDefaultImageStateByProductId(productid, false); /// Before setting default image set remaining images default value false
                    bool flag = _imagedetailsaccess.SetDefaultImage(id, isdefault);
                    message.status = "s";
                    TempData["alertMessage"] = "Product picture set Successfully";
                }
            }
            catch (Exception ex)
            {
                message.status = "e";
                message.body = ex.Message;
                throw ex;
            }

            return Json(message);
            ;
        }

        [HttpGet]
        public JsonResult SetFeaturedProduct(long productid, bool isfeatured)
        {
            var message = new Message();
            try
            {
                if (productid != 0)
                {
                    bool flag = _productaccess.SetFeaturedProduct(productid, isfeatured);
                    message.status = "s";
                    TempData["alertMessage"] = "Featured Product picture set Successfully";
                }
            }
            catch (Exception ex)
            {
                message.status = "e";
                message.body = ex.Message;
                throw ex;
            }
            return Json(message);
        }

        #region Client front end
        [HttpGet]
        public IActionResult ListProductsByCategory(long categoryid)
        {
            try
            {
                var Categories = _productcategoryaccess.ProductCategoryList(1, 20);
                var Products = _productaccess.FeaturedProductList(1, 10);

                if (categoryid != 0)
                {
                    Products = _productaccess.GetProductListByCategoryId(categoryid);
                }

                var DefaultProductListViewModel = new DefaultProductListViewModel
                {
                    Categories = Categories,
                    Products = Products
                };

                return View(DefaultProductListViewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //front end
        [HttpGet]
        public IActionResult ProductDetails(long id)
        {
            //if (String.IsNullOrEmpty(id))
            if (id!=0)
            {
                return View();
            }
            else
            {
                var product = _productaccess.GetProduct(id);
                var productimages = _productaccess.GetProductImageListByProductIdAndImageDesc(id, "P");
                var wcareimages = _productaccess.GetProductImageListByProductIdAndImageDesc(id, "P");

                var ProductPicturesViewModel = new ProductPicturesViewModel
                {
                    Product = product,
                    ProductImages = productimages,
                    WcareImages = wcareimages
                };

                return View(ProductPicturesViewModel);
            }
        }

        [HttpGet]
        public JsonResult ListProductsByCategoryId(long categoryid)
        {
            var message = new Message();
            try
            {
                if (categoryid != 0)
                {
                    var Products = _productaccess.GetProductListByCategoryId(categoryid);
                    var Categories = _productcategoryaccess.ProductCategoryList(1, 20);
                    var DefaultProductListViewModel = new DefaultProductListViewModel
                    {
                        Categories = Categories,
                        Products = Products
                    };
                    message.status = "s";
                    TempData["alertMessage"] = "Featured Product picture set Successfully";
                    return Json(DefaultProductListViewModel);
                }
            }
            catch (Exception ex)
            {
                message.status = "e";
                message.body = ex.Message;
                throw ex;
            }
            return Json(message);


        }

        #endregion
    }
}
