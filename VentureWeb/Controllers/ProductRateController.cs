﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VentureWeb.DAL;
using VentureWeb.Models;

namespace VentureWeb.Controllers
{
    public class ProductRateController : Controller
    {
        private readonly ILogger<ProductRateController> _logger;
        private readonly IProductRateaccess _productrateaccess;
        public ProductRateController(ILogger<ProductRateController> logger, IProductRateaccess productrateaccess)
        {
            _logger = logger;
            _productrateaccess = productrateaccess;
        }

        [HttpGet]
        public IActionResult AddEditProductRate(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                TempData["Mode"] = "Add";
                return View();
            }
            else
            {
                TempData["Mode"] = "Edit";
                var productrate = _productrateaccess.GetProductRate(id);
                return View(productrate);
            }
        }

        [HttpPost]
        public IActionResult ProductRate(ProductRate model, bool add)
        {

            try
            {
                if (add)
                {
                    if (model.RateId == null)
                        throw new Exception("Enter ProductRate Id.");
                    if (model.Rate == null)
                        throw new Exception("Enter ProductRate Name.");
                    _productrateaccess.AddProductRate(model);
                    TempData["alertMessage"] = "ProductRate added Successfully";
                }
                else
                {
                    _productrateaccess.EditProductRate(model);

                    TempData["alertMessage"] = "ProductRate Updated Successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ListProductRate");
        }

        [HttpGet]
        public IActionResult ListProductRate()
        {
            try
            {
                var productrateList = _productrateaccess.ProductRateList(1, 10);
                return View(productrateList);
            }
            catch (Exception)
            {

                throw;
            }

        }


        [HttpGet]
        public IActionResult Delete(string id)
        {
            try
            {
                if (id != "")
                {

                    bool flag = _productrateaccess.DeleteProductRate(id);
                    TempData["alertMessage"] = "Deleted Successfully";

                }


            }
            catch (Exception ex)
            {

                throw ex;
            }

            //  return View(personalDetail);
            return RedirectToAction("ListProductRate");
        }
    }
}
