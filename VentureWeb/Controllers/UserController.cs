﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VentureWeb.DAL;
using VentureWeb.Models;
using VentureWeb.ViewModel;
using VentureWeb.Helpers;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text;
using System.Web;
using System.Security.Cryptography;
using System.Diagnostics;
using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Http;


namespace VentureWeb.Controllers
{
    public class UserController : Controller
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUseraccess _useraccess;
        public UserController(ILogger<UserController> logger, IUseraccess useraccess)
        {
            _logger = logger;
            _useraccess = useraccess;
        }

        [HttpGet]
        public IActionResult Registration(string id)
        {    
            var UserViewModel = new UserViewModel();         
            if (String.IsNullOrEmpty(id))
            {
                var userStatus = Enum.GetValues(typeof(UserStatus)).Cast<UserStatus>().Select(v => new SelectListItem
                {
                    Text = v.ToString(),
                    Value = ((int)v).ToString()
                }).ToList();
                UserViewModel.UserState = userStatus;
                return View(UserViewModel);
            }
            else
            {
                var user = _useraccess.GetUser(id);
                var userStatus = Enum.GetValues(typeof(UserStatus)).Cast<UserStatus>().Select(v => new SelectListItem
                {
                    Text = v.ToString(),
                    Value = ((int)v).ToString()
                }).ToList();
                userStatus.First(x => x.Value == user.UserStatus.ToString()).Selected = true;
                UserViewModel.UserState = userStatus;               
               

                //  ViewBag.userStatus = userStatus;
                UserViewModel.Registration = user;
               //UserViewModel.UserState = userStatus;
                return View(UserViewModel);
            }
        }

        [HttpPost]
        public IActionResult Registration(Registration model,bool add)
        {

            try
            {
                if (add)
                {
                    if (model.EmpNo == null)
                        throw new Exception("Enter Employee Id.");
                    if (model.EmailId == null)
                        throw new Exception("Enter Email Id.");
                    _useraccess.AddUser(model);
                    TempData["alertMessage"] = "Registerd Successfully";
                }
                else
                {
                    _useraccess.EditUser(model);

                    TempData["alertMessage"] = "Updated Successfully";
                }
                //return RedirectToAction("ListUsers");
            }
            catch (Exception ex)
            {
                TempData["alertMessage"] = ex.Message.ToString();
                //  throw ex;
                //return View();
            }

            return RedirectToAction("ListUsers");
        }

        [HttpGet]
        public IActionResult ListUsers()

        {
            try
            {
                var userList = _useraccess.UserList(1, 10);
                return View(userList);
            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpGet]
        public IActionResult Delete(string id)
        {
            try
            {
                if (id != "")
                {
                    
                    bool flag = _useraccess.DeleteUser(id);
                    TempData["alertMessage"] = "Deleted Successfully";

                }
               
               
            }
            catch (Exception ex)
            {

                throw ex;
            }

            //  return View(personalDetail);
            return RedirectToAction("ListUsers");
        }

        [HttpGet]
        public IActionResult ResetPassword(string id)
        {
            if (String.IsNullOrEmpty(id))
            {

                return View();
            }
            else
            {
                ViewBag.EmpId = id;
                var user = _useraccess.GetUser(id);
                return View(user);
            }
        }

        [HttpPost]
        public ActionResult ResetPassword(Registration model)
        {            
            try
            {
                bool resetResponse = _useraccess.ResetPassword(model.EmpNo, model.Password);
                if (resetResponse)
                {
                    TempData["alertMessage"] = "Successfully Changed";
                }
                else
                {
                    TempData["alertMessage"] = "Something went horribly wrong!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
           
        }

        [HttpGet]
        public IActionResult UserRegistration()
        {
            return View();           
        }

        [HttpPost]
        public IActionResult UserRegistration(Registration model, bool add)
        {
            try
            {
                if (add)
                {
                    if (model.EmpNo == null)
                        throw new Exception("Enter Employee Id.");
                    if (model.EmailId == null)
                        throw new Exception("Enter Email Id.");
                    _useraccess.AddUser(model);
                    TempData["alertMessage"] = "Registerd Successfully";
                }
                else
                {
                    _useraccess.EditUser(model);
                    TempData["alertMessage"] = "Updated Successfully";
                }
                //return RedirectToAction("ListUsers");
            }
            catch (Exception ex)
            {
                TempData["alertMessage"] = ex.Message.ToString();
                //  throw ex;               
            }
            return RedirectToAction("ListUsers");
        }


        public IActionResult UserLogin()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            var data = _useraccess.GetUserByEmailIdandPassword(username, password);
            if (data.Count() > 0)
            {
                HttpContext.Session.SetString("username", data.FirstOrDefault().Name);
                HttpContext.Session.SetString("userId", data.FirstOrDefault().EmpNo);


                return RedirectToAction("Home", "Home");
            }
           
            else
            {
                ViewBag.error = "Invalid Account";
                return View("UserLogin");
            }
        }

        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("username");
            return RedirectToAction("UserLogin");
        }


    }
}
