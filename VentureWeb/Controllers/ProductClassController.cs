﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VentureWeb.DAL;
using VentureWeb.Models;

namespace VentureWeb.Controllers
{
    public class ProductClassController : Controller
    {
        private readonly ILogger<ProductClassController> _logger;
        private readonly IProductClassAccess _productclassaccess;
        public ProductClassController(ILogger<ProductClassController> logger, IProductClassAccess productclassaccess)
        {
            _logger = logger;
            _productclassaccess = productclassaccess;
        }

        [HttpGet]
        public IActionResult AddEditProductClass(string id)
        {
            try
            {
                if (String.IsNullOrEmpty(id))
                {
                    TempData["Mode"] = "Add";
                    return View();
                }
                else
                {
                    TempData["Mode"] = "Edit";
                    var productclass = _productclassaccess.GetProductClass(id);
                    return View(productclass);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public IActionResult ProductClass(ProductClass model, bool add)
        {

            try
            {
                model.EntryUserDate =DateTime.Now;
                model.UpdateUserDate = DateTime.Now;
                model.EntryUserId =Convert.ToInt32(HttpContext.Session.Keys.Select(x=>x=="userId").FirstOrDefault());

                if (add)
                {
                    if (model.ProductClassId == null)
                        throw new Exception("Enter ProductClass Id.");
                    if (model.ProductClassName == null)
                        throw new Exception("Enter ProductClass Name.");
                    _productclassaccess.AddProductClass(model);
                    TempData["alertMessage"] = "productclass added Successfully";
                }
                else
                {
                    _productclassaccess.EditProductClass(model);

                    TempData["alertMessage"] = "ProductClass Updated Successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ListProductClass");
        }

        [HttpGet]
        public IActionResult ListProductClass()
        {
            try
            {
                var productclassList = _productclassaccess.ProductClassList(1, 10);
                return View(productclassList);
            }
            catch (Exception)
            {

                throw;
            }

        }


        [HttpGet]
        public IActionResult Delete(long id)
        {
            try
            {
                if (id != 0)
                {

                    bool flag = _productclassaccess.DeleteProductClass(id);
                    TempData["alertMessage"] = "Deleted Successfully";

                }


            }
            catch (Exception ex)
            {

                throw ex;
            }

            //  return View(personalDetail);
            return RedirectToAction("ListProductClass");
        }

    }
}
