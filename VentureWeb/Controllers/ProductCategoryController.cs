﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VentureWeb.DAL;
using VentureWeb.Models;


namespace VentureWeb.Controllers
{
    public class ProductCategoryController : Controller
    {
        private readonly ILogger<ProductCategoryController> _logger;
        private readonly IProductCategoryaccess _productcategoryaccess;
        public ProductCategoryController(ILogger<ProductCategoryController> logger, IProductCategoryaccess productcategoryaccess)
        {
            _logger = logger;
            _productcategoryaccess = productcategoryaccess;
        }

        [HttpGet]
        public IActionResult AddEditProductCategory(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                TempData["Mode"] = "Add";
                return View();
            }
            else
            {
                TempData["Mode"] = "Edit";
                var productcategory = _productcategoryaccess.GetProductCategory(id);
                return View(productcategory);
            }
        }

        [HttpPost]
        public IActionResult ProductCategory(ProductCategory model, bool add)
        {

            try
            {
                if (add)
                {
                    if (model.ProductCategoryId == null)
                        throw new Exception("Enter Product Category Id.");
                    if (model.ProductCategoryName == null)
                        throw new Exception("Enter ProductCategory Name.");
                    _productcategoryaccess.AddProductCategory(model);
                    TempData["alertMessage"] = "Product Category added Successfully";
                }
                else
                {
                    _productcategoryaccess.EditProductCategory(model);

                    TempData["alertMessage"] = "Product Category Updated Successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ListProductCategory");
        }

        [HttpGet]
        public IActionResult ListProductCategory()

        {
            try
            {
                var productcategoryList = _productcategoryaccess.ProductCategoryList(1, 10);
                return View(productcategoryList);
            }
            catch (Exception)
            {

                throw;
            }

        }

   

        [HttpGet]
        public IActionResult Delete(long id)
        {
            try
            {
                if (id != 0)
                {
                    
                    bool flag = _productcategoryaccess.DeleteProductCategory(id);
                    TempData["alertMessage"] = "Deleted Successfully";

                }
               
               
            }
            catch (Exception ex)
            {

                throw ex;
            }

            //  return View(personalDetail);
            return RedirectToAction("ListProductCategory");
        }
                  
    }
}
