﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VentureWeb.DAL;
using VentureWeb.Models;

namespace VentureWeb.Controllers
{
    public class CompanyController : Controller
    {
        private readonly ILogger<CompanyController> _logger;
        private readonly ICompanyaccess _companyaccess;
        public CompanyController(ILogger<CompanyController> logger, ICompanyaccess companyaccess)
        {
            _logger = logger;
            _companyaccess = companyaccess;
        }

        [HttpGet]
        public IActionResult AddEditCompany(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                TempData["Mode"] = "Add";
                return View();
            }
            else
            {
                TempData["Mode"] = "Edit";
                var company = _companyaccess.GetCompany(id);
                return View(company);
            }
        }

        [HttpPost]
        public IActionResult Company(Company model, bool add)
        {

            try
            {
                if (add)
                {
                    if (model.CompId == null)
                        throw new Exception("Enter Company Id.");
                    if (model.CompName == null)
                        throw new Exception("Enter Company Name.");
                    _companyaccess.AddCompany(model);
                    TempData["alertMessage"] = "company added Successfully";
                }
                else
                {
                    _companyaccess.EditCompany(model);

                    TempData["alertMessage"] = "Company Updated Successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("ListCompany");
        }

        [HttpGet]
        public IActionResult ListCompany()

        {
            try
            {
                var companyList = _companyaccess.CompanyList(1, 10);
                return View(companyList);
            }
            catch (Exception)
            {

                throw;
            }

        }


        [HttpGet]
        public IActionResult Delete(string id)
        {
            try
            {
                if (id != "")
                {

                    bool flag = _companyaccess.DeleteCompany(id);
                    TempData["alertMessage"] = "Deleted Successfully";

                }


            }
            catch (Exception ex)
            {

                throw ex;
            }

            //  return View(personalDetail);
            return RedirectToAction("ListCompany");
        }

    }
}
