﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace VentureWeb.Migrations
{
    public partial class VentureWeb3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProductRate",
                columns: table => new
                {
                    RateId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CompId = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    EffectDate = table.Column<DateTime>(nullable: false),
                    Rate = table.Column<decimal>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    DyeRate = table.Column<decimal>(nullable: false),
                    TotalRate = table.Column<decimal>(nullable: false),
                    EntryUserId = table.Column<int>(nullable: false),
                    EntryUserDate = table.Column<DateTime>(nullable: false),
                    UpdateUserId = table.Column<int>(nullable: false),
                    UpdateUserDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductRate", x => x.RateId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProductRate");
        }
    }
}
