﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace VentureWeb.Migrations
{
    public partial class VentureWed1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    ProductId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CompId = table.Column<int>(nullable: false),
                    ProductClassId = table.Column<int>(nullable: false),
                    ProductCategoryId = table.Column<int>(nullable: false),
                    ProductName = table.Column<string>(nullable: true),
                    Width = table.Column<string>(nullable: true),
                    Width1 = table.Column<int>(nullable: false),
                    Width2 = table.Column<int>(nullable: false),
                    Width3 = table.Column<int>(nullable: false),
                    WidthTotal = table.Column<int>(nullable: false),
                    ReferenceCode = table.Column<string>(nullable: true),
                    ITCCode = table.Column<string>(nullable: true),
                    BarCode = table.Column<string>(nullable: true),
                    ValidationMethod = table.Column<int>(nullable: false),
                    OpeningUnitId = table.Column<int>(nullable: false),
                    AlertUnitId = table.Column<int>(nullable: false),
                    AlertUnitMethod = table.Column<int>(nullable: false),
                    AlertUnitFactor = table.Column<decimal>(nullable: false),
                    SaleUnitId = table.Column<int>(nullable: false),
                    FreeQuantityTag = table.Column<int>(nullable: false),
                    MinimumLevel = table.Column<decimal>(nullable: false),
                    MaximunLevel = table.Column<decimal>(nullable: false),
                    ReorderLevel = table.Column<decimal>(nullable: false),
                    DefaultPurchaseRate = table.Column<decimal>(nullable: false),
                    DefaultSateRate = table.Column<decimal>(nullable: false),
                    MinimumSaleRate = table.Column<decimal>(nullable: false),
                    MaximumSaleRate = table.Column<decimal>(nullable: false),
                    TaxPercent = table.Column<decimal>(nullable: false),
                    IsFinishedProduct = table.Column<int>(nullable: false),
                    EntryUserId = table.Column<int>(nullable: false),
                    EntryUserDate = table.Column<DateTime>(nullable: false),
                    UpdateUserId = table.Column<int>(nullable: false),
                    UpdateUserDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.ProductId);
                });

            migrationBuilder.CreateTable(
                name: "ProductCategory",
                columns: table => new
                {
                    ProductCategoryId = table.Column<string>(nullable: false),
                    ProductCategoryName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCategory", x => x.ProductCategoryId);
                });

            migrationBuilder.CreateTable(
                name: "Registration",
                columns: table => new
                {
                    EmpId = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Designation = table.Column<string>(nullable: true),
                    ReportingTo = table.Column<string>(nullable: true),
                    EmailId = table.Column<string>(nullable: true),
                    PhoneNo = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Registration", x => x.EmpId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "ProductCategory");

            migrationBuilder.DropTable(
                name: "Registration");
        }
    }
}
