﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace VentureWeb.Migrations
{
    public partial class VentureWen22 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Company",
                columns: table => new
                {
                    CompId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CompName = table.Column<string>(nullable: true),
                    CompShortName = table.Column<string>(nullable: true),
                    CompPerAdd1 = table.Column<string>(nullable: true),
                    CompPerAdd2 = table.Column<string>(nullable: true),
                    CompPerAdd3 = table.Column<string>(nullable: true),
                    CompMailAdd1 = table.Column<string>(nullable: true),
                    CompMailAdd2 = table.Column<string>(nullable: true),
                    CompMailAdd3 = table.Column<string>(nullable: true),
                    CompPanNo = table.Column<string>(nullable: true),
                    CompTanNo = table.Column<string>(nullable: true),
                    CompListNo = table.Column<string>(nullable: true),
                    CompCstNo = table.Column<string>(nullable: true),
                    CompVatFlg = table.Column<string>(nullable: true),
                    CompBookMstDt = table.Column<DateTime>(nullable: false),
                    CompPhone1 = table.Column<string>(nullable: true),
                    CompPhone2 = table.Column<string>(nullable: true),
                    CompPhone3 = table.Column<string>(nullable: true),
                    CompPhone4 = table.Column<string>(nullable: true),
                    CompFaxNo1 = table.Column<string>(nullable: true),
                    CompFaxNo2 = table.Column<string>(nullable: true),
                    CompEmailId1 = table.Column<string>(nullable: true),
                    CompEmailId2 = table.Column<string>(nullable: true),
                    CompWebsite = table.Column<string>(nullable: true),
                    CompStatus = table.Column<string>(nullable: true),
                    EntryUserId = table.Column<int>(nullable: false),
                    EntryUserDate = table.Column<DateTime>(nullable: false),
                    UpdateUserId = table.Column<int>(nullable: false),
                    UpdateUserDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company", x => x.CompId);
                });

            migrationBuilder.CreateTable(
                name: "ProductClass",
                columns: table => new
                {
                    ProductClassId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CompId = table.Column<int>(nullable: false),
                    ProductClassName = table.Column<string>(nullable: true),
                    UnderId = table.Column<int>(nullable: false),
                    IsParent = table.Column<string>(nullable: true),
                    ParentString = table.Column<string>(nullable: true),
                    IsMain = table.Column<int>(nullable: false),
                    EntryUserId = table.Column<int>(nullable: false),
                    EntryUserDate = table.Column<DateTime>(nullable: false),
                    UpdateUserId = table.Column<int>(nullable: false),
                    UpdateUserDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductClass", x => x.ProductClassId);
                });

            migrationBuilder.CreateTable(
                name: "Unit",
                columns: table => new
                {
                    UnitId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CompId = table.Column<int>(nullable: false),
                    UnitName = table.Column<string>(nullable: true),
                    UnitDescription = table.Column<string>(nullable: true),
                    UnitDecimalPlace = table.Column<int>(nullable: false),
                    EntryUserId = table.Column<int>(nullable: false),
                    EntryUserDate = table.Column<DateTime>(nullable: false),
                    UpdateUserId = table.Column<int>(nullable: false),
                    UpdateUserDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Unit", x => x.UnitId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Company");

            migrationBuilder.DropTable(
                name: "ProductClass");

            migrationBuilder.DropTable(
                name: "Unit");
        }
    }
}
