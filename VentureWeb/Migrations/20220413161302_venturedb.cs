﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace VentureWeb.Migrations
{
    public partial class venturedb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Company",
                columns: table => new
                {
                    CompId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CompName = table.Column<string>(nullable: true),
                    CompShortName = table.Column<string>(nullable: true),
                    CompPerAdd1 = table.Column<string>(nullable: true),
                    CompPerAdd2 = table.Column<string>(nullable: true),
                    CompPerAdd3 = table.Column<string>(nullable: true),
                    CompMailAdd1 = table.Column<string>(nullable: true),
                    CompMailAdd2 = table.Column<string>(nullable: true),
                    CompMailAdd3 = table.Column<string>(nullable: true),
                    CompPanNo = table.Column<string>(nullable: true),
                    CompTanNo = table.Column<string>(nullable: true),
                    CompListNo = table.Column<string>(nullable: true),
                    CompCstNo = table.Column<string>(nullable: true),
                    CompVatFlg = table.Column<string>(nullable: true),
                    CompBookMstDt = table.Column<DateTime>(nullable: false),
                    CompPhone1 = table.Column<string>(nullable: true),
                    CompPhone2 = table.Column<string>(nullable: true),
                    CompPhone3 = table.Column<string>(nullable: true),
                    CompPhone4 = table.Column<string>(nullable: true),
                    CompFaxNo1 = table.Column<string>(nullable: true),
                    CompFaxNo2 = table.Column<string>(nullable: true),
                    CompEmailId1 = table.Column<string>(nullable: true),
                    CompEmailId2 = table.Column<string>(nullable: true),
                    CompWebsite = table.Column<string>(nullable: true),
                    CompStatus = table.Column<string>(nullable: true),
                    EntryUserId = table.Column<long>(nullable: false),
                    EntryUserDate = table.Column<DateTime>(nullable: false),
                    UpdateUserId = table.Column<long>(nullable: false),
                    UpdateUserDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company", x => x.CompId);
                });

            migrationBuilder.CreateTable(
                name: "ImageDetails",
                columns: table => new
                {
                    ImageId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ImageType = table.Column<string>(nullable: true),
                    LinkId = table.Column<long>(nullable: false),
                    ImagePath = table.Column<string>(nullable: true),
                    ImageName = table.Column<string>(nullable: true),
                    ImageExt = table.Column<string>(nullable: true),
                    ImageDesc = table.Column<string>(nullable: true),
                    IsDefault = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImageDetails", x => x.ImageId);
                });

            migrationBuilder.CreateTable(
                name: "Ledger",
                columns: table => new
                {
                    LedgerId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CompId = table.Column<long>(nullable: false),
                    LedgerName = table.Column<string>(nullable: true),
                    LedgerGroupId = table.Column<long>(nullable: false),
                    InventoryEffect = table.Column<int>(nullable: false),
                    IsMain = table.Column<int>(nullable: false),
                    IsSubledger = table.Column<int>(nullable: false),
                    AgentId = table.Column<long>(nullable: false),
                    CountryId = table.Column<long>(nullable: false),
                    Accountent = table.Column<string>(nullable: true),
                    Ledgeraddress1 = table.Column<string>(nullable: true),
                    Ledgeraddress2 = table.Column<string>(nullable: true),
                    Ledgeraddress3 = table.Column<string>(nullable: true),
                    Ledgerphone1 = table.Column<string>(nullable: true),
                    Ledgerphone2 = table.Column<string>(nullable: true),
                    Ledgerfax = table.Column<string>(nullable: true),
                    Ledgeremail = table.Column<string>(nullable: true),
                    LedgerWebsite = table.Column<string>(nullable: true),
                    LedgerVatNo = table.Column<string>(nullable: true),
                    LedgerPanNo = table.Column<string>(nullable: true),
                    BankBranch = table.Column<string>(nullable: true),
                    Interestmethod = table.Column<string>(nullable: true),
                    InterestPtc = table.Column<decimal>(nullable: false),
                    SbuLedgerTypeId = table.Column<long>(nullable: false),
                    SubledgerDestination = table.Column<string>(nullable: true),
                    SubledgerClassId = table.Column<long>(nullable: false),
                    SubledgerContactPerson = table.Column<string>(nullable: true),
                    SubledgerCstNo = table.Column<string>(nullable: true),
                    SubledgerCreditDays = table.Column<int>(nullable: false),
                    SubledgerCreditLimit = table.Column<int>(nullable: false),
                    taxNature = table.Column<string>(nullable: true),
                    Showledger = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    DateOfMarraige = table.Column<DateTime>(nullable: false),
                    manualStopBuilding = table.Column<int>(nullable: false),
                    DeducteeType = table.Column<int>(nullable: false),
                    StateId = table.Column<long>(nullable: false),
                    SectionId = table.Column<long>(nullable: false),
                    OnlyAccounts = table.Column<int>(nullable: false),
                    EntryUserId = table.Column<long>(nullable: false),
                    EntryUserDate = table.Column<DateTime>(nullable: false),
                    UpdateUserId = table.Column<long>(nullable: false),
                    UpdateUserDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ledger", x => x.LedgerId);
                });

            migrationBuilder.CreateTable(
                name: "LedgerGroup",
                columns: table => new
                {
                    LedgerGroupId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CompId = table.Column<long>(nullable: false),
                    LedgerGroupName = table.Column<string>(nullable: true),
                    UnderId = table.Column<long>(nullable: false),
                    IsParent = table.Column<string>(nullable: true),
                    Parentstring = table.Column<string>(nullable: true),
                    IsMain = table.Column<int>(nullable: false),
                    IsEditable = table.Column<int>(nullable: false),
                    IsAddress = table.Column<int>(nullable: false),
                    IsTrading = table.Column<int>(nullable: false),
                    GroupType = table.Column<string>(nullable: true),
                    IsSubledger = table.Column<int>(nullable: false),
                    IsMiscelaneous = table.Column<int>(nullable: false),
                    ShowLedgergroup = table.Column<string>(nullable: true),
                    Is_Pl = table.Column<int>(nullable: false),
                    EntryUserId = table.Column<long>(nullable: false),
                    EntryUserDate = table.Column<DateTime>(nullable: false),
                    UpdateUserId = table.Column<long>(nullable: false),
                    UpdateUserDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LedgerGroup", x => x.LedgerGroupId);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    ProductId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CompId = table.Column<long>(nullable: false),
                    ProductClassId = table.Column<long>(nullable: false),
                    ProductCategoryId = table.Column<long>(nullable: false),
                    ProductName = table.Column<string>(nullable: true),
                    Width = table.Column<string>(nullable: true),
                    Width1 = table.Column<int>(nullable: false),
                    Width2 = table.Column<int>(nullable: false),
                    Width3 = table.Column<int>(nullable: false),
                    WidthTotal = table.Column<int>(nullable: false),
                    ReferenceCode = table.Column<string>(nullable: true),
                    ITCCode = table.Column<string>(nullable: true),
                    BarCode = table.Column<string>(nullable: true),
                    ValidationMethod = table.Column<int>(nullable: false),
                    OpeningUnitId = table.Column<int>(nullable: false),
                    AlertUnitId = table.Column<long>(nullable: false),
                    AlertUnitMethod = table.Column<int>(nullable: false),
                    AlertUnitFactor = table.Column<decimal>(nullable: false),
                    SaleUnitId = table.Column<long>(nullable: false),
                    FreeQuantityTag = table.Column<int>(nullable: false),
                    MinimumLevel = table.Column<decimal>(nullable: false),
                    MaximunLevel = table.Column<decimal>(nullable: false),
                    ReorderLevel = table.Column<decimal>(nullable: false),
                    DefaultPurchaseRate = table.Column<decimal>(nullable: false),
                    DefaultSateRate = table.Column<decimal>(nullable: false),
                    MinimumSaleRate = table.Column<decimal>(nullable: false),
                    MaximumSaleRate = table.Column<decimal>(nullable: false),
                    TaxPercent = table.Column<decimal>(nullable: false),
                    IsFinishedProduct = table.Column<int>(nullable: false),
                    EntryUserId = table.Column<long>(nullable: false),
                    EntryUserDate = table.Column<DateTime>(nullable: false),
                    UpdateUserId = table.Column<long>(nullable: false),
                    UpdateUserDate = table.Column<DateTime>(nullable: false),
                    IsFeatured = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.ProductId);
                });

            migrationBuilder.CreateTable(
                name: "ProductCategory",
                columns: table => new
                {
                    ProductCategoryId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ProductCategoryName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCategory", x => x.ProductCategoryId);
                });

            migrationBuilder.CreateTable(
                name: "ProductClass",
                columns: table => new
                {
                    ProductClassId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CompId = table.Column<int>(nullable: false),
                    ProductClassName = table.Column<string>(nullable: true),
                    UnderId = table.Column<long>(nullable: false),
                    IsParent = table.Column<string>(nullable: true),
                    ParentString = table.Column<string>(nullable: true),
                    IsMain = table.Column<int>(nullable: false),
                    EntryUserId = table.Column<long>(nullable: false),
                    EntryUserDate = table.Column<DateTime>(nullable: false),
                    UpdateUserId = table.Column<long>(nullable: false),
                    UpdateUserDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductClass", x => x.ProductClassId);
                });

            migrationBuilder.CreateTable(
                name: "ProductRate",
                columns: table => new
                {
                    RateId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CompId = table.Column<long>(nullable: false),
                    ProductId = table.Column<long>(nullable: false),
                    EffectDate = table.Column<DateTime>(nullable: false),
                    Rate = table.Column<decimal>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    DyeRate = table.Column<decimal>(nullable: false),
                    TotalRate = table.Column<decimal>(nullable: false),
                    EntryUserId = table.Column<long>(nullable: false),
                    EntryUserDate = table.Column<DateTime>(nullable: false),
                    UpdateUserId = table.Column<long>(nullable: false),
                    UpdateUserDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductRate", x => x.RateId);
                });

            migrationBuilder.CreateTable(
                name: "Registration",
                columns: table => new
                {
                    EmpId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true),
                    Designation = table.Column<string>(nullable: true),
                    ReportingTo = table.Column<string>(nullable: true),
                    EmailId = table.Column<string>(nullable: true),
                    EmpNo = table.Column<string>(nullable: true),
                    PhoneNo = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    UserStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Registration", x => x.EmpId);
                });

            migrationBuilder.CreateTable(
                name: "SubLedgerClass",
                columns: table => new
                {
                    SubLedgerClassId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CompId = table.Column<long>(nullable: false),
                    SubLedgerClassName = table.Column<string>(nullable: true),
                    UnderId = table.Column<long>(nullable: false),
                    IsParent = table.Column<string>(nullable: true),
                    ParentString = table.Column<string>(nullable: true),
                    IsMain = table.Column<int>(nullable: false),
                    UnderLevel = table.Column<int>(nullable: false),
                    MainId = table.Column<long>(nullable: false),
                    EntryUserId = table.Column<long>(nullable: false),
                    EntryUserDate = table.Column<DateTime>(nullable: false),
                    UpdateUserId = table.Column<long>(nullable: false),
                    UpdateUserDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubLedgerClass", x => x.SubLedgerClassId);
                });

            migrationBuilder.CreateTable(
                name: "Unit",
                columns: table => new
                {
                    UnitId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CompId = table.Column<long>(nullable: false),
                    UnitName = table.Column<string>(nullable: true),
                    UnitDescription = table.Column<string>(nullable: true),
                    UnitDecimalPlace = table.Column<int>(nullable: false),
                    EntryUserId = table.Column<long>(nullable: false),
                    EntryUserDate = table.Column<DateTime>(nullable: false),
                    UpdateUserId = table.Column<long>(nullable: false),
                    UpdateUserDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Unit", x => x.UnitId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Company");

            migrationBuilder.DropTable(
                name: "ImageDetails");

            migrationBuilder.DropTable(
                name: "Ledger");

            migrationBuilder.DropTable(
                name: "LedgerGroup");

            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "ProductCategory");

            migrationBuilder.DropTable(
                name: "ProductClass");

            migrationBuilder.DropTable(
                name: "ProductRate");

            migrationBuilder.DropTable(
                name: "Registration");

            migrationBuilder.DropTable(
                name: "SubLedgerClass");

            migrationBuilder.DropTable(
                name: "Unit");
        }
    }
}
